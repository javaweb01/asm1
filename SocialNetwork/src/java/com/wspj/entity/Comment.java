/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wspj.entity;

/**
 *
 * @author nam oio
 */
public class Comment {
    String cm_avatar;
    String cm_idpost;
     String cm_username;
      String cm_content;
       String cm_time;

    public Comment() {
    }

    public Comment(String cm_avatar, String cm_idpost, String cm_username, String cm_content, String cm_time) {
        this.cm_avatar = cm_avatar;
        this.cm_idpost = cm_idpost;
        this.cm_username = cm_username;
        this.cm_content = cm_content;
        this.cm_time = cm_time;
    }

    public String getCm_avatar() {
        return cm_avatar;
    }

    public void setCm_avatar(String cm_avatar) {
        this.cm_avatar = cm_avatar;
    }

    public String getCm_idpost() {
        return cm_idpost;
    }

    public void setCm_idpost(String cm_idpost) {
        this.cm_idpost = cm_idpost;
    }

    public String getCm_username() {
        return cm_username;
    }

    public void setCm_username(String cm_username) {
        this.cm_username = cm_username;
    }

    public String getCm_content() {
        return cm_content;
    }

    public void setCm_content(String cm_content) {
        this.cm_content = cm_content;
    }

    public String getCm_time() {
        return cm_time;
    }

    public void setCm_time(String cm_time) {
        this.cm_time = cm_time;
    }

    
    
}