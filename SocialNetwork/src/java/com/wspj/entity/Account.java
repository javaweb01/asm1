/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wspj.entity;

/**
 *
 * @author nam oio
 */
public class Account {
    String ac_username ,ac_password,ac_permission,ac_fullname,ac_birthday,ac_avatar;

    public Account() {
    }

    public Account(String ac_username, String ac_password, String ac_permission, String ac_fullname, String ac_birthday, String ac_avatar) {
        this.ac_username = ac_username;
        this.ac_password = ac_password;
        this.ac_permission = ac_permission;
        this.ac_fullname = ac_fullname;
        this.ac_birthday = ac_birthday;
        this.ac_avatar = ac_avatar;
    }

    public String getAc_username() {
        return ac_username;
    }

    public void setAc_username(String ac_username) {
        this.ac_username = ac_username;
    }

    public String getAc_password() {
        return ac_password;
    }

    public void setAc_password(String ac_password) {
        this.ac_password = ac_password;
    }

    public String getAc_permission() {
        return ac_permission;
    }

    public void setAc_permission(String ac_permission) {
        this.ac_permission = ac_permission;
    }

    public String getAc_fullname() {
        return ac_fullname;
    }

    public void setAc_fullname(String ac_fullname) {
        this.ac_fullname = ac_fullname;
    }

    public String getAc_birthday() {
        return ac_birthday;
    }

    public void setAc_birthday(String ac_birthday) {
        this.ac_birthday = ac_birthday;
    }

    public String getAc_avatar() {
        return ac_avatar;
    }

    public void setAc_avatar(String ac_avatar) {
        this.ac_avatar = ac_avatar;
    }
    
    
}
