package com.wspj.entity;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


/**
 *
 * @author nam oio
 */
public class post {
 String   post_id  ;
String post_username ;
String post_content  ;
String post_image ;
String post_time ;

    public post() {
    }

    public post(String post_id, String post_username, String post_content, String post_image, String post_time) {
        this.post_id = post_id;
        this.post_username = post_username;
        this.post_content = post_content;
        this.post_image = post_image;
        this.post_time = post_time;
    }

    public String getPost_id() {
        return post_id;
    }

    public void setPost_id(String post_id) {
        this.post_id = post_id;
    }

    public String getPost_username() {
        return post_username;
    }

    public void setPost_username(String post_username) {
        this.post_username = post_username;
    }

    public String getPost_content() {
        return post_content;
    }

    public void setPost_content(String post_content) {
        this.post_content = post_content;
    }

    public String getPost_image() {
        return post_image;
    }

    public void setPost_image(String post_image) {
        this.post_image = post_image;
    }

    public String getPost_time() {
        return post_time;
    }

    public void setPost_time(String post_time) {
        this.post_time = post_time;
    }


}
