/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wpsj.controller;

import com.wpsj.model.getAll;
import com.wspj.entity.Account;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author nam oio
 */
public class Login extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
             String ac_username = request.getParameter("username");
        String ac_password = request.getParameter("password");
        getAll all=new getAll();
        all.checkLogin(ac_username, ac_password);
         HttpSession session = request.getSession();
        Account account=new Account();
       List<Account> a; 
        a=   all.checkLogin(ac_username, ac_password);
        String checklg="";
        for (Account check : a) {
            if(check.getAc_username()!=null){
                
               
session.setAttribute("username", check.getAc_username());
String file=check.getAc_avatar();
         file=file.replace("\\","/");
session.setAttribute("avatar",file );
                
       checklg=check.getAc_permission();
                System.out.println("ttttttt"+check.getAc_permission());
            }
        }
            if(checklg.equals("user")){
             RequestDispatcher rd = request.getRequestDispatcher("newjsp.jsp");
        rd.forward(request,response);
        }
         if(checklg.equals("admin")){
             RequestDispatcher rd = request.getRequestDispatcher("admin_listAccount.jsp");
        rd.forward(request,response);
        }
        if(checklg.equals("")){
        RequestDispatcher rd = request.getRequestDispatcher("login.jsp");
        rd.forward(request,response);
    
    }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
