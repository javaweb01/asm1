package com.wpsj.controller;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

 
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

 
import com.google.gson.Gson;
import com.wpsj.da.ProductDataAccess;
import com.wspj.entity.detail;
import javax.servlet.http.HttpSession;
 
 
/**
 * Servlet implementation class GetDistrictFromProvince
 */
@WebServlet("/GetDistrictFromProvince")
public class GetDistrictFromProvince extends HttpServlet {
        private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public GetDistrictFromProvince() {
        super();
        // TODO Auto-generated constructor stub
    }

 
        /**
         * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
         */
        protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
                // TODO Auto-generated method stub
              detail detai=new detail();
                
                          HttpSession session = request.getSession();
            
String post_username=session.getAttribute("username").toString();
                String cm = request.getParameter("comment");
                 String id = request.getParameter("id");
                if(cm!=""){
                    ProductDataAccess addcm=new ProductDataAccess();
                    
                  if(addcm.setComment(id,post_username,cm,"04-02-2018")){
                      
                 
                String json = null;   
                String date= java.time.LocalDate.now().toString();
                String time=java.time.LocalTime.now().toString();
                String dt=date+" "+time;
                detai.setComment(cm);
                detai.setTime(dt);
                detai.setName(post_username);
                response.setContentType("application/json");
                response.setCharacterEncoding("UTF-8");
               
                        json = new Gson().toJson(detai);
               
                response.getWriter().write(json);
                }
                }
                
        }

 
        /**
         * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
         */
        protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
                // TODO Auto-generated method stub
                doGet(request, response);
        }
} 