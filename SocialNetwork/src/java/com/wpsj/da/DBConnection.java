/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wpsj.da;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Puarinnnn
 */
public class DBConnection {
    private static Connection connection=null;
    public Connection getConnection() throws ClassNotFoundException,
            SQLException,
            InstantiationException{
        try {
            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver").newInstance();
        } catch (IllegalAccessException ex) {
            Logger.getLogger(DBConnection.class.getName()).log(Level.SEVERE, null, ex);
        }
            String url = "jdbc:sqlserver://localhost:1433;databaseName=SocialNetwork";
            String username = "sa";
            String password = "123456";
             connection = DriverManager.getConnection(url, username, password);
          
        return connection;
    }
}
