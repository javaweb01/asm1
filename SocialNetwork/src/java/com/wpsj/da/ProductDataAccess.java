/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wpsj.da;

import com.wspj.entity.Account;
import com.wspj.entity.Comment;
import static java.lang.System.out;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Puarinnnn
 */
public class ProductDataAccess {

    private PreparedStatement searchStatement;

    public List<post> getProducts() {
        List<post> list = new LinkedList<post>();
        try {
            try {
                try {
                    Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver").newInstance();
                } catch (ClassNotFoundException ex) {
                    Logger.getLogger(ProductDataAccess.class.getName()).log(Level.SEVERE, null, ex);
                }
            } catch (InstantiationException ex) {
                Logger.getLogger(ProductDataAccess.class.getName()).log(Level.SEVERE, null, ex);
            } catch (IllegalAccessException ex) {
                Logger.getLogger(ProductDataAccess.class.getName()).log(Level.SEVERE, null, ex);
            }
            String url = "jdbc:sqlserver://localhost:1433;databaseName=SocialNetwork";
            String username = "sa";
            String password = "123456";
            Connection connection = DriverManager.getConnection(url, username, password);
            System.out.println("Connected: " + connection.getClientInfo());
            java.sql.Statement st = connection.createStatement();
            String query = "SELECT  * \n"
                    + "FROM tbl_account T\n"
                    + "LEFT OUTER JOIN  tbl_post T1 \n"
                    + "ON T.ac_username=T1.post_username";
            ResultSet rs = st.executeQuery(query);

            if (rs != null) {
                while (rs.next()) {
                    post ps = new post();
                    ps.setPost_content(rs.getString("post_content"));
                    ps.setPost_avatar(rs.getString("ac_avatar"));
                    ps.setPost_id(rs.getString("post_id"));
                    ps.setPost_image(rs.getString("post_image"));
                    ps.setPost_time(rs.getString("post_time"));
                    ps.setPost_username(rs.getString("post_username"));
                    list.add(ps);
                    System.out.println(rs.getString("post_content"));
                }
            }

            rs.close();
            st.close();
            connection.close();
            Collections.reverse(list);
            return list;
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }

    }

    public boolean setComment(String idpost, String user, String content, String time) {
        try {

            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver").newInstance();
            String url = "jdbc:sqlserver://localhost:1433;databaseName=SocialNetwork";
            String username = "sa";
            String password = "123456";
            Connection con = DriverManager.getConnection(url, username, password);
            System.out.println("Connected: " + con.getClientInfo());
            java.sql.Statement st = con.createStatement();

            PreparedStatement pst = con.prepareStatement("insert into tbl_comment values(?,?,?,?)");
            pst.setString(1, idpost);
            pst.setString(2, user);
            pst.setString(3, content);
            pst.setString(4, time);

            pst.executeUpdate();
            return true;
        } catch (Exception e) {
            out.println(e);
            return false;
        }

    }

    public List<post> getPostId(String id) {
        List<post> list = new LinkedList<post>();
        try {
            try {
                try {
                    Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver").newInstance();
                } catch (ClassNotFoundException ex) {
                    Logger.getLogger(ProductDataAccess.class.getName()).log(Level.SEVERE, null, ex);
                }
            } catch (InstantiationException ex) {
                Logger.getLogger(ProductDataAccess.class.getName()).log(Level.SEVERE, null, ex);
            } catch (IllegalAccessException ex) {
                Logger.getLogger(ProductDataAccess.class.getName()).log(Level.SEVERE, null, ex);
            }
            String url = "jdbc:sqlserver://localhost:1433;databaseName=SocialNetwork";
            String username = "sa";
            String password = "123456";
            Connection connection = DriverManager.getConnection(url, username, password);
            System.out.println("Connected: " + connection.getClientInfo());
            java.sql.Statement st = connection.createStatement();
            String query = "select * from tbl_post where post_id='" + id + "'";

            ResultSet rs = st.executeQuery(query);

            if (rs != null) {
                while (rs.next()) {
                    post ps = new post();
                    ps.setPost_content(rs.getString("post_content"));
                    System.out.println(rs.getString("post_id"));
                    ps.setPost_id(rs.getString("post_id"));
                    ps.setPost_image(rs.getString("post_image"));
                    ps.setPost_time(rs.getString("post_time"));
                    ps.setPost_username(rs.getString("post_username"));
                    list.add(ps);

                }
            }

            rs.close();
            st.close();
            connection.close();
            Collections.reverse(list);
            return list;
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }

    }

    public List<post> getPostUser(String user) {
        List<post> list = new LinkedList<post>();
        try {
            try {
                try {
                    Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver").newInstance();
                } catch (ClassNotFoundException ex) {
                    Logger.getLogger(ProductDataAccess.class.getName()).log(Level.SEVERE, null, ex);
                }
            } catch (InstantiationException ex) {
                Logger.getLogger(ProductDataAccess.class.getName()).log(Level.SEVERE, null, ex);
            } catch (IllegalAccessException ex) {
                Logger.getLogger(ProductDataAccess.class.getName()).log(Level.SEVERE, null, ex);
            }
            String url = "jdbc:sqlserver://localhost:1433;databaseName=SocialNetwork";
            String username = "sa";
            String password = "123456";
            Connection connection = DriverManager.getConnection(url, username, password);
            System.out.println("Connected: " + connection.getClientInfo());
            java.sql.Statement st = connection.createStatement();
            String query = "SELECT  * \n"
                    + "FROM tbl_account T\n"
                    + "LEFT OUTER JOIN  tbl_post T1 \n"
                    + "ON T.ac_username=T1.post_username where ac_username='" + user + "'";

            ResultSet rs = st.executeQuery(query);

            if (rs != null) {
                while (rs.next()) {
                    post ps = new post();
                    ps.setPost_content(rs.getString("post_content"));
                    System.out.println("kaka" + rs.getString("post_id"));
                    ps.setPost_avatar(rs.getString("ac_avatar"));
                    ps.setPost_id(rs.getString("post_id"));
                    ps.setPost_image(rs.getString("post_image"));
                    ps.setPost_time(rs.getString("post_time"));
                    ps.setPost_username(rs.getString("post_username"));
                    list.add(ps);

                }
            }

            rs.close();
            st.close();
            connection.close();
            Collections.reverse(list);
            return list;
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }

    }

    public List<Comment> getCmtId(String id) {
        List<Comment> list = new LinkedList<Comment>();
        try {
            try {
                try {
                    Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver").newInstance();
                } catch (ClassNotFoundException ex) {
                    Logger.getLogger(ProductDataAccess.class.getName()).log(Level.SEVERE, null, ex);
                }
            } catch (InstantiationException ex) {
                Logger.getLogger(ProductDataAccess.class.getName()).log(Level.SEVERE, null, ex);
            } catch (IllegalAccessException ex) {
                Logger.getLogger(ProductDataAccess.class.getName()).log(Level.SEVERE, null, ex);
            }
            String url = "jdbc:sqlserver://localhost:1433;databaseName=SocialNetwork";
            String username = "sa";
            String password = "123456";
            Connection connection = DriverManager.getConnection(url, username, password);
            System.out.println("Connected: " + connection.getClientInfo());
            java.sql.Statement st = connection.createStatement();
            String query = "SELECT  * \n"
                    + "FROM tbl_comment T\n"
                    + "LEFT OUTER JOIN  tbl_post T1 \n"
                    + "ON T.cm_idpost=T1.post_id\n"
                    + "LEFT OUTER JOIN  tbl_account T2\n"
                    + "ON T.cm_username=T2.ac_username \n"
                    + "where cm_idpost='" + id + "'";

            ResultSet rs = st.executeQuery(query);

            if (rs != null) {
                while (rs.next()) {
                    Comment ps = new Comment();
                    ps.setCm_avatar(rs.getString("ac_avatar"));
                    ps.setCm_content(rs.getString("cm_content"));
                    System.out.println("cm" + rs.getString("cm_idpost"));
                    ps.setCm_idpost(rs.getString("cm_idpost"));
                    ps.setCm_username(rs.getString("cm_username"));
                    ps.setCm_time(rs.getString("cm_time"));

                    list.add(ps);

                }
            }

            rs.close();
            st.close();
            connection.close();
            Collections.reverse(list);
            Collections.reverse(list);
            return list;
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }

    }

    public List<Account> checkLogin(String user, String pas) {
        List<Account> list = new LinkedList<Account>();
        try {
            try {
                try {
                    Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver").newInstance();
                } catch (ClassNotFoundException ex) {
                    Logger.getLogger(ProductDataAccess.class.getName()).log(Level.SEVERE, null, ex);
                }
            } catch (InstantiationException ex) {
                Logger.getLogger(ProductDataAccess.class.getName()).log(Level.SEVERE, null, ex);
            } catch (IllegalAccessException ex) {
                Logger.getLogger(ProductDataAccess.class.getName()).log(Level.SEVERE, null, ex);
            }
            String url = "jdbc:sqlserver://localhost:1433;databaseName=SocialNetwork";
            String username = "sa";
            String password = "123456";
            Connection connection = DriverManager.getConnection(url, username, password);
            System.out.println("Connected: " + connection.getClientInfo());
            java.sql.Statement st = connection.createStatement();
            String query = "select * from tbl_account where ac_username='" + user + "' and ac_password='" + pas + "'";;

            ResultSet rs = st.executeQuery(query);

            if (rs != null) {
                while (rs.next()) {
                    Account ps = new Account();
                    ps.setAc_username(rs.getString("ac_username"));

                    ps.setAc_fullname(rs.getString("ac_fullname"));
                    ps.setAc_password(rs.getString("ac_password"));
                    ps.setAc_permission(rs.getString("ac_permission"));
                    ps.setAc_avatar(rs.getString("ac_avatar"));
                    ps.setAc_birthday(rs.getString("ac_birthday"));
                    list.add(ps);

                }
            }

            rs.close();
            st.close();
            connection.close();
            Collections.reverse(list);
            Collections.reverse(list);
            return list;
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }

    }

    public boolean upProfile(String name, String file) {
        try {

            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver").newInstance();
            String url = "jdbc:sqlserver://localhost:1433;databaseName=SocialNetwork";
            String username = "sa";
            String password = "123456";
            Connection con = DriverManager.getConnection(url, username, password);
            System.out.println("Connected: " + con.getClientInfo());
            java.sql.Statement st = con.createStatement();

            PreparedStatement pst = con.prepareStatement("UPDATE tbl_account SET ac_avatar ='" + file + "' WHERE ac_username ='" + name + "'");

            pst.executeUpdate();

            return true;
        } catch (Exception e) {
            out.println(e);
            return false;
        }
    }

    public boolean upAcount(String user, String pass, String fullname) {
        try {

            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver").newInstance();
            String url = "jdbc:sqlserver://localhost:1433;databaseName=SocialNetwork";
            String username = "sa";
            String password = "123456";
            Connection con = DriverManager.getConnection(url, username, password);
            System.out.println("Connected: " + con.getClientInfo());
            java.sql.Statement st = con.createStatement();

            PreparedStatement pst = con.prepareStatement(" UPDATE tbl_account SET ac_password ='" + pass + "' , ac_fullname='" + fullname + "' WHERE ac_username ='" + user + "'");

            pst.executeUpdate();

            return true;
        } catch (Exception e) {
            out.println(e);
            return false;
        }
    }

    public List<Account> getAccount() {
        List<Account> list = new LinkedList<Account>();
        try {
            try {
                try {
                    Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver").newInstance();
                } catch (ClassNotFoundException ex) {
                    Logger.getLogger(ProductDataAccess.class.getName()).log(Level.SEVERE, null, ex);
                }
            } catch (InstantiationException ex) {
                Logger.getLogger(ProductDataAccess.class.getName()).log(Level.SEVERE, null, ex);
            } catch (IllegalAccessException ex) {
                Logger.getLogger(ProductDataAccess.class.getName()).log(Level.SEVERE, null, ex);
            }
            String url = "jdbc:sqlserver://localhost:1433;databaseName=SocialNetwork";
            String username = "sa";
            String password = "123456";
            Connection connection = DriverManager.getConnection(url, username, password);
            System.out.println("Connected: " + connection.getClientInfo());
            java.sql.Statement st = connection.createStatement();
            String query = "select * from tbl_account ";;

            ResultSet rs = st.executeQuery(query);

            if (rs != null) {
                while (rs.next()) {
                    Account ps = new Account();
                    ps.setAc_username(rs.getString("ac_username"));
                    ps.setAc_fullname(rs.getString("ac_fullname"));
                    ps.setAc_password(rs.getString("ac_password"));
                    ps.setAc_permission(rs.getString("ac_permission"));
                    ps.setAc_avatar(rs.getString("ac_avatar"));
                    ps.setAc_birthday(rs.getString("ac_birthday"));
                    list.add(ps);

                }
            }

            rs.close();
            st.close();
            connection.close();
            Collections.reverse(list);
            Collections.reverse(list);
            return list;
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }

    }

    public Account getAccount(String name) {

        try {
            try {
                try {
                    Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver").newInstance();
                } catch (ClassNotFoundException ex) {
                    Logger.getLogger(ProductDataAccess.class.getName()).log(Level.SEVERE, null, ex);
                }
            } catch (InstantiationException ex) {
                Logger.getLogger(ProductDataAccess.class.getName()).log(Level.SEVERE, null, ex);
            } catch (IllegalAccessException ex) {
                Logger.getLogger(ProductDataAccess.class.getName()).log(Level.SEVERE, null, ex);
            }
            String url = "jdbc:sqlserver://localhost:1433;databaseName=SocialNetwork";
            String username = "sa";
            String password = "123456";
            Connection connection = DriverManager.getConnection(url, username, password);
            System.out.println("Connected: " + connection.getClientInfo());
            java.sql.Statement st = connection.createStatement();
            String query = "select * from tbl_account ";;

            ResultSet rs = st.executeQuery(query);
            Account ac = new Account();
            if (rs != null) {
                while (rs.next()) {

                    ac.setAc_username(rs.getString("ac_username"));
                    ac.setAc_fullname(rs.getString("ac_fullname"));
                    ac.setAc_password(rs.getString("ac_password"));
                    ac.setAc_permission(rs.getString("ac_permission"));
                    ac.setAc_avatar(rs.getString("ac_avatar"));
                    ac.setAc_birthday(rs.getString("ac_birthday"));

                }
            }

            rs.close();
            st.close();
            connection.close();

            return ac;
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

    public boolean deletePost(String id) {
        try {

            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver").newInstance();
            String url = "jdbc:sqlserver://localhost:1433;databaseName=SocialNetwork";
            String username = "sa";
            String password = "123456";
            Connection con = DriverManager.getConnection(url, username, password);
            System.out.println("Connected: " + con.getClientInfo());
            java.sql.Statement st = con.createStatement();

            PreparedStatement pst = con.prepareStatement("delete from tbl_post where post_id='" + id + "'");

            pst.executeUpdate();

            return true;
        } catch (Exception e) {
            out.println(e);
            return false;
        }
    }

    public boolean deleteAcount(String user) {
        try {
            deleteCommentAc(user);
            deletePostAc(user);

            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver").newInstance();
            String url = "jdbc:sqlserver://localhost:1433;databaseName=SocialNetwork";
            String username = "sa";
            String password = "123456";
            Connection con = DriverManager.getConnection(url, username, password);
            System.out.println("Connected: " + con.getClientInfo());
            java.sql.Statement st = con.createStatement();

            PreparedStatement pst = con.prepareStatement("delete from tbl_account where ac_username='" + user + "'");

            pst.executeUpdate();

            return true;
        } catch (Exception e) {
            out.println(e);
            return false;
        }
    }

    public boolean deleteCommentAc(String user) {
        try {

            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver").newInstance();
            String url = "jdbc:sqlserver://localhost:1433;databaseName=SocialNetwork";
            String username = "sa";
            String password = "123456";
            Connection con = DriverManager.getConnection(url, username, password);
            System.out.println("Connected: " + con.getClientInfo());
            java.sql.Statement st = con.createStatement();

            PreparedStatement pst = con.prepareStatement("delete from tbl_comment where cm_username='" + user + "'");

            pst.executeUpdate();

            return true;
        } catch (Exception e) {
            out.println(e);
            return false;
        }
    }

    public boolean deletePostAc(String user) {
        try {

            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver").newInstance();
            String url = "jdbc:sqlserver://localhost:1433;databaseName=SocialNetwork";
            String username = "sa";
            String password = "123456";
            Connection con = DriverManager.getConnection(url, username, password);
            System.out.println("Connected: " + con.getClientInfo());
            java.sql.Statement st = con.createStatement();

            PreparedStatement pst = con.prepareStatement("delete from tbl_post where post_username='" + user + "'");

            pst.executeUpdate();

            return true;
        } catch (Exception e) {
            out.println(e);
            return false;
        }
    }

    public boolean createAccount(String user, String pas, String name) {
        try {

            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver").newInstance();
            String url = "jdbc:sqlserver://localhost:1433;databaseName=SocialNetwork";
            String username = "sa";
            String password = "123456";
            Connection con = DriverManager.getConnection(url, username, password);
            System.out.println("Connected: " + con.getClientInfo());
            java.sql.Statement st = con.createStatement();
            PreparedStatement pst = con.prepareStatement("insert into tbl_account values(?,?,?,?,?,?)");
            pst.setString(1, user);
            pst.setString(2, pas);
            pst.setString(3, "user");
            pst.setString(4, name);
            pst.setString(5, "");
            pst.setString(6, "");
            pst.executeUpdate();
            return true;
        } catch (Exception e) {
            out.println(e);
            return false;
        }

    }
}
