package org.apache.jsp;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;

public final class login_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List<String> _jspx_dependants;

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public java.util.List<String> getDependants() {
    return _jspx_dependants;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");

      out.write("<div class=\"cont\">\n");
      out.write("  <div class=\"main-box\">\n");
      out.write("    <div class=\"log\">\n");
      out.write("      <form action=\"Login\">\n");
      out.write("        <h2>Login</h2>\n");
      out.write("        <i class=\"fa fa-fw fa-user\" aria-hidden=\"true\"></i>\n");
      out.write("        <input type=\"text\"\n");
      out.write("               value=\"");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${username}", java.lang.String.class, (PageContext)_jspx_page_context, null));
      out.write("\"\n");
      out.write("               name=\"username\"\n");
      out.write("               class=\"user\"\n");
      out.write("               placeholder=\"Username\"/>\n");
      out.write("\n");
      out.write("        <i class=\"fa fa-fw fa-lock\" aria-hidden=\"true\"></i>\n");
      out.write("        <input type=\"password\" value=\"");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${password}", java.lang.String.class, (PageContext)_jspx_page_context, null));
      out.write("\"\n");
      out.write("               name=\"password\"\n");
      out.write("               class=\"pass\"\n");
      out.write("               placeholder=\"Password\"/>\n");
      out.write("\n");
      out.write("        <button                class=\"login\">LOGIN</button>\n");
      out.write("       \n");
      out.write("      </form>\n");
      out.write("         <a href=\"Register.jsp\">Register</a>\n");
      out.write("    </div>\n");
      out.write("  </div> \n");
      out.write("</div>\n");
      out.write("\n");
      out.write("<link rel=\"stylesheet\" href=\"path/to/font-awesome/css/font-awesome.min.css\">\n");
      out.write("<link rel=\"stylesheet prefetch\" href=\"//maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css\">\n");
      out.write("\n");
      out.write("<style>\n");
      out.write("    \n");
      out.write("    @import url(\"https://fonts.googleapis.com/css?family=Raleway:300,400,700\");\n");
      out.write("\n");
      out.write("* {\n");
      out.write(" \n");
      out.write("  padding: 0;\n");
      out.write("  outline: none;\n");
      out.write("  box-sizing: border-box;\n");
      out.write("  font-family: \"Raleway\", sans-serif;\n");
      out.write("}\n");
      out.write("\n");
      out.write("\n");
      out.write("body {\n");
      out.write("  background: #41cac0;\n");
      out.write("  \n");
      out.write("}\n");
      out.write("a{\n");
      out.write("    color: #41cac0;\n");
      out.write("}\n");
      out.write(".cont {\n");
      out.write("  position: relative;\n");
      out.write("  width: 28%;\n");
      out.write("  height: 550px;\n");
      out.write("  margin: 10px auto;\n");
      out.write("}\n");
      out.write("\n");
      out.write(".main-box {\n");
      out.write("  width: 100%;\n");
      out.write("  height: 100%;\n");
      out.write("  text-align: center;\n");
      out.write("  background: #1d4a5f;\n");
      out.write("  box-shadow: 0px 0px 40px 1px #262626;\n");
      out.write("}\n");
      out.write("\n");
      out.write(".log {\n");
      out.write("  position: relative;\n");
      out.write("  top: 20%;\n");
      out.write("  width: 100%;\n");
      out.write("  height: 75%;\n");
      out.write("  padding: 18px 15px;\n");
      out.write("}\n");
      out.write("\n");
      out.write("h2,\n");
      out.write(".user,\n");
      out.write(".pass {\n");
      out.write("  display: block;\n");
      out.write("}\n");
      out.write("\n");
      out.write("h2 {\n");
      out.write("  text-align: center;\n");
      out.write("  color: #4fdac3;\n");
      out.write("  font-weight: 600;\n");
      out.write("  font-size: 30px;\n");
      out.write("  letter-spacing: 3px;\n");
      out.write("  margin-bottom: 50px;\n");
      out.write("}\n");
      out.write("\n");
      out.write(".user,\n");
      out.write(".pass {\n");
      out.write("  width: 85%;\n");
      out.write("  height: 45px;\n");
      out.write("  border: none;\n");
      out.write("  color: white;\n");
      out.write("  margin: auto;\n");
      out.write("  padding-left: 40px;\n");
      out.write("  font-size: 18px;\n");
      out.write("  font-weight: 500;\n");
      out.write("  margin-bottom: 25px;\n");
      out.write("  background: #0e2d3f;\n");
      out.write("}\n");
      out.write("\n");
      out.write("input::placeholder {\n");
      out.write("  color: #eaeaea;\n");
      out.write("  font-size: 15px;\n");
      out.write("  font-weight: lighter;\n");
      out.write("}\n");
      out.write("\n");
      out.write(".fa {\n");
      out.write("  position: relative;\n");
      out.write("  color: #fff;\n");
      out.write("  font-size: 16px;\n");
      out.write("  left: -36%;\n");
      out.write("  top: 33px;\n");
      out.write("}\n");
      out.write("\n");
      out.write(".login {\n");
      out.write("  color: #fff;\n");
      out.write("  cursor: pointer;\n");
      out.write("  width: 85%;\n");
      out.write("  height: 50px;\n");
      out.write("  border: none;\n");
      out.write("  font-size: 22px;\n");
      out.write("  font-weight: 500;\n");
      out.write("  margin-top: 2%;\n");
      out.write("  box-shadow: 0px 2px 3px 5px rgba(42, 42, 42, 0.23);\n");
      out.write("  background: linear-gradient(to right, #4ce3c2, #35c1c0);\n");
      out.write("}\n");
      out.write("\n");
      out.write("@media only screen and (min-width: 280px) {\n");
      out.write("  .cont {\n");
      out.write("    width: 90%;\n");
      out.write("  }\n");
      out.write("}\n");
      out.write("\n");
      out.write("@media only screen and (min-width: 480px) {\n");
      out.write("  .cont {\n");
      out.write("    width: 60%;\n");
      out.write("  }\n");
      out.write("}\n");
      out.write("\n");
      out.write("@media only screen and (min-width: 768px) {\n");
      out.write("  .cont {\n");
      out.write("    width: 40%;\n");
      out.write("  }\n");
      out.write("}\n");
      out.write("\n");
      out.write("@media only screen and (min-width: 992px) {\n");
      out.write("  .cont {\n");
      out.write("    width: 30%;\n");
      out.write("  }\n");
      out.write("}\n");
      out.write("\n");
      out.write("</style>");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
