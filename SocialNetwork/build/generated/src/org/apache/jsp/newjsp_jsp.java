package org.apache.jsp;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;

public final class newjsp_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List<String> _jspx_dependants;

  private org.apache.jasper.runtime.TagHandlerPool _jspx_tagPool_c_forEach_var_items;
  private org.apache.jasper.runtime.TagHandlerPool _jspx_tagPool_c_if_test;

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public java.util.List<String> getDependants() {
    return _jspx_dependants;
  }

  public void _jspInit() {
    _jspx_tagPool_c_forEach_var_items = org.apache.jasper.runtime.TagHandlerPool.getTagHandlerPool(getServletConfig());
    _jspx_tagPool_c_if_test = org.apache.jasper.runtime.TagHandlerPool.getTagHandlerPool(getServletConfig());
  }

  public void _jspDestroy() {
    _jspx_tagPool_c_forEach_var_items.release();
    _jspx_tagPool_c_if_test.release();
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html;charset=UTF-8");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");

      out.write("\n");
      out.write("\n");
      out.write("\n");
      if (_jspx_meth_c_if_0(_jspx_page_context))
        return;
      out.write('\n');
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }

  private boolean _jspx_meth_c_if_0(PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    HttpSession session = _jspx_page_context.getSession();
    ServletContext application = _jspx_page_context.getServletContext();
    HttpServletRequest request = (HttpServletRequest)_jspx_page_context.getRequest();
    //  c:if
    org.apache.taglibs.standard.tag.rt.core.IfTag _jspx_th_c_if_0 = (org.apache.taglibs.standard.tag.rt.core.IfTag) _jspx_tagPool_c_if_test.get(org.apache.taglibs.standard.tag.rt.core.IfTag.class);
    _jspx_th_c_if_0.setPageContext(_jspx_page_context);
    _jspx_th_c_if_0.setParent(null);
    _jspx_th_c_if_0.setTest(((java.lang.Boolean) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${username!=null}", java.lang.Boolean.class, (PageContext)_jspx_page_context, null)).booleanValue());
    int _jspx_eval_c_if_0 = _jspx_th_c_if_0.doStartTag();
    if (_jspx_eval_c_if_0 != javax.servlet.jsp.tagext.Tag.SKIP_BODY) {
      do {
        out.write("\n");
        out.write("    \n");
        out.write("    \n");
        out.write("<link href=\"cssmenu/csmenu.css\" rel=\"stylesheet\" type=\"text/css\"/>\n");
        out.write("<script src='https://ajax.googleapis.com/ajax/libs/jquery/1.5.1/jquery.min.js' type='text/javascript'></script>\n");
        out.write("<script src=\"cssmenu/jsmenu.js\" type=\"text/javascript\"></script>\n");
        out.write("<link href='https://fonts.googleapis.com/css?family=Raleway:400,700' rel='stylesheet' type='text/css'>\n");
        out.write("<link href=\"//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css\" rel=\"stylesheet\">\n");
        out.write("<div class=\"header\">\n");
        out.write("     <nav id=\"fixNav\" class=\"container\">\n");
        out.write("        <a class=\"fb\" href=\"#\"><i class=\"fa fa-home\" aria-hidden=\"true\"></i> </a>\n");
        out.write("     \n");
        out.write("        <a class=\"fl\" href=\"#\"><i class=\"fa fa-bolt\" aria-hidden=\"true\"></i></a>\n");
        out.write("        <a class=\"tu\" href=\"#\"><i class=\"fa fa-tumblr\"></i></a>\n");
        out.write("        <a  id='SignIn' class=\"vi\" href=\"#\"><i class=\"fa fa-pencil\" aria-hidden=\"true\"></i></a>\n");
        out.write("    </nav>\n");
        out.write("    <div class=\"inner\">\n");
        out.write("        <div class=\"user-ui\">\n");
        out.write("            <div class=\"user-menu-toggle\">\n");
        out.write("                <div class=\"profile-img\" style=\"background-image:url(");
        out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${avatar}", java.lang.String.class, (PageContext)_jspx_page_context, null));
        out.write(")\"></div>\n");
        out.write("                <span id=\"btn1\" class=\"simple-arrow fa fa-chevron-up\"></span>\n");
        out.write("            </div>\n");
        out.write("            <!-- User menu -->\n");
        out.write("            <div id='comment-editor' style='display:none !important' class=\"user-menu\">\n");
        out.write("                <div class=\"user-info\">\n");
        out.write("                    <div class=\"profile-img\"  style=\"background-image:url(");
        out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${avatar}", java.lang.String.class, (PageContext)_jspx_page_context, null));
        out.write(")\"></div>\n");
        out.write("                    <h3 class=\"name\">");
        out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${username}", java.lang.String.class, (PageContext)_jspx_page_context, null));
        out.write("</h3>\n");
        out.write("                    <div class=\"ui btn normal\"> <a href=\"SignOut\" class=\"button delete\"><span>Sign Out</span></a>\n");
        out.write("                    </div>\n");
        out.write("                </div>\n");
        out.write("                <div class=\"menu-nav\">\n");
        out.write("                    <li></span>Personal page</li>\n");
        out.write("                    <li ><a id='ed' href=\"#\">Edit</a></li>\n");
        out.write("                    <li>My comment\n");
        out.write("                    </li>\n");
        out.write("                    <li></span>My post</li>\n");
        out.write("                </div>\n");
        out.write("            </div>\n");
        out.write("        </div>\n");
        out.write("    </div>\n");
        out.write("</div>\n");
        out.write("\n");
        out.write("\n");
        out.write("\n");
        out.write("\n");
        out.write("\n");
        out.write("    <!DOCTYPE html>\n");
        out.write("\n");
        out.write("    <head>\n");
        out.write("         <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">\n");
        out.write("    </head>\n");
        out.write("        <style>\n");
        out.write("            #fixNav{\n");
        out.write("\n");
        out.write("\n");
        out.write("                margin-left: 42%;\n");
        out.write("    float: left;\n");
        out.write("    margin-top: 10px;\n");
        out.write("              width: 300px;\n");
        out.write("            }\n");
        out.write("        </style>\n");
        out.write("\n");
        out.write("        <link rel=\"stylesheet\" href=\"path/to/font-awesome/css/font-awesome.min.css\">\n");
        out.write("        <link rel=\"stylesheet prefetch\" href=\"//maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css\">\n");
        out.write("\n");
        out.write("   \n");
        out.write("\n");
        out.write("    <style>\n");
        out.write("        .container {\n");
        out.write("            width: 100%;\n");
        out.write("            display: table;\n");
        out.write("            margin: 0;\n");
        out.write("        }\n");
        out.write("\n");
        out.write("        nav a {\n");
        out.write("            display: table-cell;\n");
        out.write("            color: white;\n");
        out.write("            width: 14.2%;\n");
        out.write("            text-align: center;\n");
        out.write("            font-size: 2em;\n");
        out.write("            padding: .5em;\n");
        out.write("        }\n");
        out.write("        @media (max-width: 768px) {\n");
        out.write("            nav a {\n");
        out.write("                font-size: 1.5em;\n");
        out.write("            }\n");
        out.write("        }\n");
        out.write("        nav a:first-child {\n");
        out.write("            border-radius: 5px 0 0 5px;\n");
        out.write("        }\n");
        out.write("        @media (max-width: 768px) {\n");
        out.write("            nav a:first-child {\n");
        out.write("                border-radius: 0;\n");
        out.write("            }\n");
        out.write("        }\n");
        out.write("        nav a:last-child {\n");
        out.write("            border-radius: 0px 5px 5px 0px;\n");
        out.write("        }\n");
        out.write("        @media (max-width: 768px) {\n");
        out.write("            nav a:last-child {\n");
        out.write("                border-radius: 0;\n");
        out.write("            }\n");
        out.write("        }\n");
        out.write("\n");
        out.write("        nav a:first-child:nth-last-child(1) {\n");
        out.write("            width: 100%;\n");
        out.write("        }\n");
        out.write("\n");
        out.write("        /* two items */\n");
        out.write("        nav a:first-child:nth-last-child(2),\n");
        out.write("        nav a:first-child:nth-last-child(2) ~ a {\n");
        out.write("            width: 50%;\n");
        out.write("        }\n");
        out.write("\n");
        out.write("        /* three items */\n");
        out.write("        nav a:first-child:nth-last-child(3),\n");
        out.write("        nav a:first-child:nth-last-child(3) ~ a {\n");
        out.write("            width: 33.3333%;\n");
        out.write("        }\n");
        out.write("\n");
        out.write("        /* four items */\n");
        out.write("        nav a:first-child:nth-last-child(4),\n");
        out.write("        nav a:first-child:nth-last-child(4) ~ a {\n");
        out.write("            width: 25%;\n");
        out.write("        }\n");
        out.write("\n");
        out.write("        /* five items */\n");
        out.write("        nav a:first-child:nth-last-child(5),\n");
        out.write("        nav a:first-child:nth-last-child(5) ~ a {\n");
        out.write("            width: 20%;\n");
        out.write("        }\n");
        out.write("\n");
        out.write("        /* six items */\n");
        out.write("        nav a:first-child:nth-last-child(6),\n");
        out.write("        nav a:first-child:nth-last-child(6) ~ a {\n");
        out.write("            width: 16.666%;\n");
        out.write("        }\n");
        out.write("\n");
        out.write("        /* seven items */\n");
        out.write("        nav a:first-child:nth-last-child(7),\n");
        out.write("        nav a:first-child:nth-last-child(7) ~ a {\n");
        out.write("            width: 14.285%;\n");
        out.write("        }\n");
        out.write("\n");
        out.write("        /* eight items */\n");
        out.write("        nav a:first-child:nth-last-child(8),\n");
        out.write("        nav a:first-child:nth-last-child(8) ~ a {\n");
        out.write("            width: 12.5%;\n");
        out.write("        }\n");
        out.write("\n");
        out.write("        /*\n");
        out.write("        Assigning link background colors and setting the hover and active states. For quick adding of buttons, perform a search + replace on the 2 letter variable name.\n");
        out.write("        */\n");
        out.write("        nav a.fb {\n");
        out.write("            background: #3B5998;\n");
        out.write("            text-shadow: 1px 1px 0px #263961;\n");
        out.write("        }\n");
        out.write("        nav a.fb:hover {\n");
        out.write("            color: #3055a3 !important;\n");
        out.write("            background: #2d4373;\n");
        out.write("        }\n");
        out.write("        nav a.fb:active {\n");
        out.write("            color: #3B5998;\n");
        out.write("            background: #1e2e4f;\n");
        out.write("        }\n");
        out.write("\n");
        out.write("        nav a.tw {\n");
        out.write("            background: #55ACEE;\n");
        out.write("            text-shadow: 1px 1px 0px #1689e0;\n");
        out.write("        }\n");
        out.write("        nav a.tw:hover {\n");
        out.write("            color: #4cadf7 !important;\n");
        out.write("            background: #2795e9;\n");
        out.write("        }\n");
        out.write("        nav a.tw:active {\n");
        out.write("            color: #55ACEE;\n");
        out.write("            background: #147bc9;\n");
        out.write("        }\n");
        out.write("\n");
        out.write("        nav a.gp {\n");
        out.write("            background: #DD4B39;\n");
        out.write("            text-shadow: 1px 1px 0px #ac2d1e;\n");
        out.write("        }\n");
        out.write("        nav a.gp:hover {\n");
        out.write("            color: #e9422d !important;\n");
        out.write("            background: #c23321;\n");
        out.write("        }\n");
        out.write("        nav a.gp:active {\n");
        out.write("            color: #DD4B39;\n");
        out.write("            background: #96271a;\n");
        out.write("        }\n");
        out.write("\n");
        out.write("        nav a.ig {\n");
        out.write("            background: #517FA4;\n");
        out.write("            text-shadow: 1px 1px 0px #385771;\n");
        out.write("        }\n");
        out.write("        nav a.ig:hover {\n");
        out.write("            color: #4580b0 !important;\n");
        out.write("            background: #406582;\n");
        out.write("        }\n");
        out.write("        nav a.ig:active {\n");
        out.write("            color: #517FA4;\n");
        out.write("            background: #2f4a60;\n");
        out.write("        }\n");
        out.write("\n");
        out.write("        nav a.fl {\n");
        out.write("            background: #FF0084;\n");
        out.write("            text-shadow: 1px 1px 0px #b3005c;\n");
        out.write("        }\n");
        out.write("        nav a.fl:hover {\n");
        out.write("            color: #ff0084 !important;\n");
        out.write("            background: #cc006a;\n");
        out.write("        }\n");
        out.write("        nav a.fl:active {\n");
        out.write("            color: #FF0084;\n");
        out.write("            background: #99004f;\n");
        out.write("        }\n");
        out.write("\n");
        out.write("        nav a.tu {\n");
        out.write("            background: #32506D;\n");
        out.write("            text-shadow: 1px 1px 0px #1a2a39;\n");
        out.write("        }\n");
        out.write("        nav a.tu:hover {\n");
        out.write("            color: #2a5075 !important;\n");
        out.write("            background: #22364a;\n");
        out.write("        }\n");
        out.write("        nav a.tu:active {\n");
        out.write("            color: #32506D;\n");
        out.write("            background: #121d27;\n");
        out.write("        }\n");
        out.write("\n");
        out.write("        nav a.vi {\n");
        out.write("            background: #00BF8F;\n");
        out.write("            text-shadow: 1px 1px 0px #007356;\n");
        out.write("        }\n");
        out.write("        nav a.vi:hover {\n");
        out.write("            color: #00bf8f !important;\n");
        out.write("            background: #008c69;\n");
        out.write("        }\n");
        out.write("        nav a.vi:active {\n");
        out.write("            color: #00BF8F;\n");
        out.write("            background: #005943;\n");
        out.write("        }\n");
        out.write("\n");
        out.write("    </style>\n");
        out.write("\n");
        out.write("</head>\n");
        out.write("<body>\n");
        out.write("\n");
        out.write("    ");
        com.wpsj.model.getAll finder1 = null;
        synchronized (request) {
          finder1 = (com.wpsj.model.getAll) _jspx_page_context.getAttribute("finder1", PageContext.REQUEST_SCOPE);
          if (finder1 == null){
            finder1 = new com.wpsj.model.getAll();
            _jspx_page_context.setAttribute("finder1", finder1, PageContext.REQUEST_SCOPE);
          }
        }
        out.write("\n");
        out.write("\n");
        out.write("\n");
        out.write("    <div class=\"container\">\n");
        out.write("        <ul class=\"posts\">\n");
        out.write("\n");
        out.write("            ");
        if (_jspx_meth_c_forEach_0((javax.servlet.jsp.tagext.JspTag) _jspx_th_c_if_0, _jspx_page_context))
          return true;
        out.write("\n");
        out.write("\n");
        out.write("        </ul>\n");
        out.write("\n");
        out.write("    </div>\n");
        out.write("\n");
        out.write("\n");
        out.write("    <link href=\"csspost/newcss.css\" rel=\"stylesheet\" type=\"text/css\"/>\n");
        out.write("    <style>\n");
        out.write("        #v{\n");
        out.write("            font-family: \"Helvetica Neue\", HelveticaNeue, Helvetica, Arial, sans-serif;\n");
        out.write("            size: 14px;\n");
        out.write("            line-height: 21px;\n");
        out.write("\n");
        out.write("            color: #444444;\n");
        out.write("            width: 514px;\n");
        out.write("            word-wrap: break-word;\n");
        out.write("        }\n");
        out.write("\n");
        out.write("\n");
        out.write("        footer  {\n");
        out.write("            width: 200px;\n");
        out.write("        }</style>\n");
        out.write("</body>\n");
        out.write("\n");
        out.write("\n");
        out.write("\n");
        out.write("\n");
        out.write("\n");
        out.write("\n");
        out.write("\n");
        out.write("\n");
        out.write("\n");
        out.write("\n");
        out.write("\n");
        out.write("\n");
        out.write("<script src=\"csscomment/jquery-3.3.1.js\" type=\"text/javascript\"></script>\n");
        out.write("<script src=\"csscomment/jquery-3.3.1.min.js\" type=\"text/javascript\"></script>\n");
        out.write("<div style='display:none !important' class='login'>\n");
        out.write("    <button class='close' id='close'></button>\n");
        out.write("    <form action=\"FileUploadServlet\" method=\"post\" enctype=\"multipart/form-data\">\n");
        out.write("\n");
        out.write("      \n");
        out.write("<textarea name=\"content\" onkeyup=\"textAreaAdjust(this)\"></textarea>\n");
        out.write("        <input name=\"file\" type='file' onchange=\"readURL(this);\" />\n");
        out.write("        <img  class=\"v\" style='display:none !important' id=\"blah\" src=\"\" alt=\"your image\" />\n");
        out.write("        <input type=\"submit\" value=\"Post\">\n");
        out.write("    </form>\n");
        out.write("</div>\n");
        out.write("\n");
        out.write("<style>\n");
        out.write("     textarea {\n");
        out.write("        margin-top: 31px;\n");
        out.write("    margin-left: 18px;\n");
        out.write("    margin-bottom: 22px;\n");
        out.write("    width: 450px;\n");
        out.write("    min-height: 60px;\n");
        out.write("    height: auto;\n");
        out.write("    /* margin: 22px auto; */\n");
        out.write("    padding: 20px;\n");
        out.write("    display: block;\n");
        out.write("    overflow: hidden;\n");
        out.write("    border-color: #ECEFF1;\n");
        out.write("    border-radius: 2px;\n");
        out.write("    font: 300 14px/20px 'Roboto',sans-serif;\n");
        out.write("    resize: none;\n");
        out.write("}\n");
        out.write("\n");
        out.write("    #blah{\n");
        out.write("        max-width:100%;\n");
        out.write("    }\n");
        out.write("    input[type=file]{\n");
        out.write("        width: 100%;\n");
        out.write("        padding:10px;\n");
        out.write("        background:#2d2d2d;}\n");
        out.write("</style>\n");
        out.write("\n");
        out.write("<script>\n");
        out.write("    function textAreaAdjust(o) {\n");
        out.write("  o.style.height = \"1px\";\n");
        out.write("  o.style.height = (5+o.scrollHeight)+\"px\";\n");
        out.write("}\n");
        out.write("    function readURL(input) {\n");
        out.write("        if (input.files && input.files[0]) {\n");
        out.write("            var reader = new FileReader();\n");
        out.write("\n");
        out.write("            reader.onload = function (e) {\n");
        out.write("                $('.v').show();\n");
        out.write("                $('#blah')\n");
        out.write("                        .attr('src', e.target.result);\n");
        out.write("            };\n");
        out.write("            reader.readAsDataURL(input.files[0]);\n");
        out.write("        }\n");
        out.write("    }\n");
        out.write("</script>\n");
        out.write("<link href=\"csscreatepost/createpost.css\" rel=\"stylesheet\" type=\"text/css\"/>\n");
        out.write("<script src=\"csscreatepost/jscreatepost.js\" type=\"text/javascript\"></script>\n");
        out.write("\n");
        out.write("\n");
        out.write("\n");
        out.write("\n");
        out.write("\n");
        out.write("\n");
        out.write("\n");
        out.write("\n");
        out.write("\n");
        out.write("\n");
        out.write("\n");
        out.write("\n");
        out.write("\n");
        out.write("\n");
        out.write("\n");
        out.write("\n");
        out.write("\n");
        out.write("\n");
        out.write("\n");
        out.write("\n");
        out.write("\n");
        out.write("\n");
        out.write("\n");
        out.write("\n");
        out.write("\n");
        out.write("\n");
        out.write("\n");
        out.write("\n");
        out.write("\n");
        out.write("<script src=\"csscomment/jquery-3.3.1.js\" type=\"text/javascript\"></script>\n");
        out.write("<script src=\"csscomment/jquery-3.3.1.min.js\" type=\"text/javascript\"></script>\n");
        out.write("<div style='display:none !important' class='prototype'>\n");
        out.write("    <button class='close' id='close1'></button>\n");
        out.write("   <form action=\"UpProfile\" method=\"post\" enctype=\"multipart/form-data\">\n");
        out.write("\n");
        out.write("<div class=\"media-container\">\n");
        out.write("\t<span class=\"media-overlay\">\n");
        out.write("\t\t<input name=\"file\" type='file' id=\"media-input\" >\n");
        out.write("\t\t<i class=\"glyphicon glyphicon-edit media-icon\"></i>\n");
        out.write("\t</span>\n");
        out.write("\t<figure class=\"media-object\">\n");
        out.write("\t\t<img class=\"img-object\" src=\"");
        out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${avatar}", java.lang.String.class, (PageContext)_jspx_page_context, null));
        out.write("\">\n");
        out.write("\t</figure>\n");
        out.write("</div>\n");
        out.write("<div class=\"media-control\">\n");
        out.write("\t<a class=\"edit-profile\">Edit Profile</a>\n");
        out.write("\t<a class=\"save-profile\">Save Profile</a>\n");
        out.write("</div>\n");
        out.write("        \n");
        out.write("        \n");
        out.write("       \n");
        out.write("        <input type=\"text\" value=\"");
        out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${username}", java.lang.String.class, (PageContext)_jspx_page_context, null));
        out.write("\" id=\"input\" placeholder=\"Name\">\n");
        out.write(" \n");
        out.write(" \n");
        out.write(" \n");
        out.write("  <input type=\"submit\" value=\"Save\" id=\"input-submit\">   \n");
        out.write("        \n");
        out.write("    </form>\n");
        out.write("</div>\n");
        out.write("\n");
        out.write("<link href=\"csseditprototype/prototype.css\" rel=\"stylesheet\" type=\"text/css\"/>\n");
        out.write("<script src=\"csseditprototype/jsprototype.js\" type=\"text/javascript\"></script>\n");
        out.write("<script>\n");
        out.write("\n");
        out.write("    var btn_save = $(\".save-profile\"),\n");
        out.write("\tbtn_edit = $(\".edit-profile\"),\n");
        out.write("\timg_object = $(\".img-object\"),\n");
        out.write("\toverlay = $(\".media-overlay\"),\n");
        out.write("\tmedia_input = $(\"#media-input\");\n");
        out.write("\n");
        out.write("btn_save.hide(0);\n");
        out.write("overlay.hide(0);\n");
        out.write("\n");
        out.write("$('.edit-profile').on('click', function() {\n");
        out.write("\t$(this).hide(0);\n");
        out.write("\t$(this).siblings().fadeToggle(300);\n");
        out.write("\toverlay.fadeToggle(300);\n");
        out.write("});\n");
        out.write("\n");
        out.write("media_input.change(function() {\n");
        out.write("\tif (this.files && this.files[0]) {\n");
        out.write("\t\tvar reader = new FileReader();\n");
        out.write("\t\t\n");
        out.write("\t\treader.onload = function(e) {\n");
        out.write("\t\t\timg_object.attr(\"src\", e.target.result);\n");
        out.write("\t\t};\n");
        out.write("\t\t\n");
        out.write("\t\treader.readAsDataURL(this.files[0]);\n");
        out.write("\t}\n");
        out.write("});\n");
        out.write("    \n");
        out.write("  \n");
        out.write("    \n");
        out.write("   \n");
        out.write("</script>\n");
        out.write("\n");
        int evalDoAfterBody = _jspx_th_c_if_0.doAfterBody();
        if (evalDoAfterBody != javax.servlet.jsp.tagext.BodyTag.EVAL_BODY_AGAIN)
          break;
      } while (true);
    }
    if (_jspx_th_c_if_0.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
      _jspx_tagPool_c_if_test.reuse(_jspx_th_c_if_0);
      return true;
    }
    _jspx_tagPool_c_if_test.reuse(_jspx_th_c_if_0);
    return false;
  }

  private boolean _jspx_meth_c_forEach_0(javax.servlet.jsp.tagext.JspTag _jspx_th_c_if_0, PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  c:forEach
    org.apache.taglibs.standard.tag.rt.core.ForEachTag _jspx_th_c_forEach_0 = (org.apache.taglibs.standard.tag.rt.core.ForEachTag) _jspx_tagPool_c_forEach_var_items.get(org.apache.taglibs.standard.tag.rt.core.ForEachTag.class);
    _jspx_th_c_forEach_0.setPageContext(_jspx_page_context);
    _jspx_th_c_forEach_0.setParent((javax.servlet.jsp.tagext.Tag) _jspx_th_c_if_0);
    _jspx_th_c_forEach_0.setItems((java.lang.Object) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${finder1.products}", java.lang.Object.class, (PageContext)_jspx_page_context, null));
    _jspx_th_c_forEach_0.setVar("product");
    int[] _jspx_push_body_count_c_forEach_0 = new int[] { 0 };
    try {
      int _jspx_eval_c_forEach_0 = _jspx_th_c_forEach_0.doStartTag();
      if (_jspx_eval_c_forEach_0 != javax.servlet.jsp.tagext.Tag.SKIP_BODY) {
        do {
          out.write("\n");
          out.write("                ");
          if (_jspx_meth_c_if_1((javax.servlet.jsp.tagext.JspTag) _jspx_th_c_forEach_0, _jspx_page_context, _jspx_push_body_count_c_forEach_0))
            return true;
          out.write("\n");
          out.write("            ");
          int evalDoAfterBody = _jspx_th_c_forEach_0.doAfterBody();
          if (evalDoAfterBody != javax.servlet.jsp.tagext.BodyTag.EVAL_BODY_AGAIN)
            break;
        } while (true);
      }
      if (_jspx_th_c_forEach_0.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
        return true;
      }
    } catch (Throwable _jspx_exception) {
      while (_jspx_push_body_count_c_forEach_0[0]-- > 0)
        out = _jspx_page_context.popBody();
      _jspx_th_c_forEach_0.doCatch(_jspx_exception);
    } finally {
      _jspx_th_c_forEach_0.doFinally();
      _jspx_tagPool_c_forEach_var_items.reuse(_jspx_th_c_forEach_0);
    }
    return false;
  }

  private boolean _jspx_meth_c_if_1(javax.servlet.jsp.tagext.JspTag _jspx_th_c_forEach_0, PageContext _jspx_page_context, int[] _jspx_push_body_count_c_forEach_0)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  c:if
    org.apache.taglibs.standard.tag.rt.core.IfTag _jspx_th_c_if_1 = (org.apache.taglibs.standard.tag.rt.core.IfTag) _jspx_tagPool_c_if_test.get(org.apache.taglibs.standard.tag.rt.core.IfTag.class);
    _jspx_th_c_if_1.setPageContext(_jspx_page_context);
    _jspx_th_c_if_1.setParent((javax.servlet.jsp.tagext.Tag) _jspx_th_c_forEach_0);
    _jspx_th_c_if_1.setTest(((java.lang.Boolean) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${product.post_username!=null}", java.lang.Boolean.class, (PageContext)_jspx_page_context, null)).booleanValue());
    int _jspx_eval_c_if_1 = _jspx_th_c_if_1.doStartTag();
    if (_jspx_eval_c_if_1 != javax.servlet.jsp.tagext.Tag.SKIP_BODY) {
      do {
        out.write("\n");
        out.write("                <li class=\"post\">\n");
        out.write("                    <div class=\"post-content\">\n");
        out.write("                        <header><a href=\"PersonalPage?username=");
        out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${product.post_username}", java.lang.String.class, (PageContext)_jspx_page_context, null));
        out.write('"');
        out.write('>');
        out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${product.post_username}", java.lang.String.class, (PageContext)_jspx_page_context, null));
        out.write("</a></header>\n");
        out.write("                            ");
        if (_jspx_meth_c_if_2((javax.servlet.jsp.tagext.JspTag) _jspx_th_c_if_1, _jspx_page_context, _jspx_push_body_count_c_forEach_0))
          return true;
        out.write("\n");
        out.write("\n");
        out.write("                        <footer> <p id=\"v\">\n");
        out.write("\n");
        out.write("                                ");
        out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${product.post_content}", java.lang.String.class, (PageContext)_jspx_page_context, null));
        out.write("\n");
        out.write("\n");
        out.write("\n");
        out.write("                            </p>\n");
        out.write("                            <a href=\"https://kamilciesla.pl\"><i class=\"fa fa-heart\" aria-hidden=\"true\"></i></a>\n");
        out.write("                            <a href=\"Comment?id=");
        out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${product.post_id}", java.lang.String.class, (PageContext)_jspx_page_context, null));
        out.write("\"><i class=\"fa fa-comment\" aria-hidden=\"true\"></i></a>\n");
        out.write("                            <a href=\"https://twitter.com/@kamciesla\"><i class=\"fa fa-paper-plane\" aria-hidden=\"true\"></i></a></footer>\n");
        out.write("                    </div><img src=\"");
        out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${product.post_avatar}", java.lang.String.class, (PageContext)_jspx_page_context, null));
        out.write("\" class=\"post-avatar\" href=\"http://kamilciesla.pl\"></img>\n");
        out.write("                </li>\n");
        out.write("                ");
        int evalDoAfterBody = _jspx_th_c_if_1.doAfterBody();
        if (evalDoAfterBody != javax.servlet.jsp.tagext.BodyTag.EVAL_BODY_AGAIN)
          break;
      } while (true);
    }
    if (_jspx_th_c_if_1.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
      _jspx_tagPool_c_if_test.reuse(_jspx_th_c_if_1);
      return true;
    }
    _jspx_tagPool_c_if_test.reuse(_jspx_th_c_if_1);
    return false;
  }

  private boolean _jspx_meth_c_if_2(javax.servlet.jsp.tagext.JspTag _jspx_th_c_if_1, PageContext _jspx_page_context, int[] _jspx_push_body_count_c_forEach_0)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  c:if
    org.apache.taglibs.standard.tag.rt.core.IfTag _jspx_th_c_if_2 = (org.apache.taglibs.standard.tag.rt.core.IfTag) _jspx_tagPool_c_if_test.get(org.apache.taglibs.standard.tag.rt.core.IfTag.class);
    _jspx_th_c_if_2.setPageContext(_jspx_page_context);
    _jspx_th_c_if_2.setParent((javax.servlet.jsp.tagext.Tag) _jspx_th_c_if_1);
    _jspx_th_c_if_2.setTest(((java.lang.Boolean) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${product.post_image!=''}", java.lang.Boolean.class, (PageContext)_jspx_page_context, null)).booleanValue());
    int _jspx_eval_c_if_2 = _jspx_th_c_if_2.doStartTag();
    if (_jspx_eval_c_if_2 != javax.servlet.jsp.tagext.Tag.SKIP_BODY) {
      do {
        out.write("\n");
        out.write("                            <img src=\"");
        out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${product.post_image}", java.lang.String.class, (PageContext)_jspx_page_context, null));
        out.write("\"/>\n");
        out.write("                        ");
        int evalDoAfterBody = _jspx_th_c_if_2.doAfterBody();
        if (evalDoAfterBody != javax.servlet.jsp.tagext.BodyTag.EVAL_BODY_AGAIN)
          break;
      } while (true);
    }
    if (_jspx_th_c_if_2.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
      _jspx_tagPool_c_if_test.reuse(_jspx_th_c_if_2);
      return true;
    }
    _jspx_tagPool_c_if_test.reuse(_jspx_th_c_if_2);
    return false;
  }
}
