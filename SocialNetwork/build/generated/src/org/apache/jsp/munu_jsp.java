package org.apache.jsp;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;

public final class munu_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List<String> _jspx_dependants;

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public java.util.List<String> getDependants() {
    return _jspx_dependants;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");

      out.write("\n");
      out.write("<button id='rdit'>Sign in</button>\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("<div style='display:none !important' class='login'>\n");
      out.write("    <button class='close' id='close'></button>\n");
      out.write("    <form action=\"FileUploadServlet\" method=\"post\" enctype=\"multipart/form-data\">\n");
      out.write("\n");
      out.write("      \n");
      out.write("<textarea name=\"content\" onkeyup=\"textAreaAdjust(this)\"></textarea>\n");
      out.write("        <input name=\"file\" type='file' onchange=\"readURL(this);\" />\n");
      out.write("        <img class=\"v\" style='display:none !important' id=\"blah\" src=\"\" alt=\"your image\" />\n");
      out.write("        <input type=\"submit\" value=\"Submit\">\n");
      out.write("    </form>\n");
      out.write("</div>\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("<script src=\"csscomment/jquery-3.3.1.js\" type=\"text/javascript\"></script>\n");
      out.write("<script src=\"csscomment/jquery-3.3.1.min.js\" type=\"text/javascript\"></script>\n");
      out.write("<div class='login'>\n");
      out.write("    <button class='close' id='close'></button>\n");
      out.write("    <form action=\"FileUploadServlet\" method=\"post\" enctype=\"multipart/form-data\">\n");
      out.write("<div class=\"media-container\">\n");
      out.write("\t<span class=\"media-overlay\">\n");
      out.write("\t\t<input type=\"file\" id=\"media-input\" accept=\"image/*\">\n");
      out.write("\t\t<i class=\"glyphicon glyphicon-edit media-icon\"></i>\n");
      out.write("\t</span>\n");
      out.write("\t<figure class=\"media-object\">\n");
      out.write("\t\t<img class=\"img-object\" src=\"http://placehold.it/200x200\">\n");
      out.write("\t</figure>\n");
      out.write("</div>\n");
      out.write("<div class=\"media-control\">\n");
      out.write("\t<a class=\"edit-profile\">Edit Profile</a>\n");
      out.write("\t<a class=\"save-profile\">Save Profile</a>\n");
      out.write("</div>\n");
      out.write("    </form>\n");
      out.write("</div>\n");
      out.write("\n");
      out.write("<link href=\"csseditprototype/prototype.css\" rel=\"stylesheet\" type=\"text/css\"/>\n");
      out.write("\n");
      out.write("<script>\n");
      out.write("\n");
      out.write("    var btn_save = $(\".save-profile\"),\n");
      out.write("\tbtn_edit = $(\".edit-profile\"),\n");
      out.write("\timg_object = $(\".img-object\"),\n");
      out.write("\toverlay = $(\".media-overlay\"),\n");
      out.write("\tmedia_input = $(\"#media-input\");\n");
      out.write("\n");
      out.write("btn_save.hide(0);\n");
      out.write("overlay.hide(0);\n");
      out.write("\n");
      out.write("$('.edit-profile').on('click', function() {\n");
      out.write("\t$(this).hide(0);\n");
      out.write("\t$(this).siblings().fadeToggle(300);\n");
      out.write("\toverlay.fadeToggle(300);\n");
      out.write("});\n");
      out.write("\n");
      out.write("media_input.change(function() {\n");
      out.write("\tif (this.files && this.files[0]) {\n");
      out.write("\t\tvar reader = new FileReader();\n");
      out.write("\t\t\n");
      out.write("\t\treader.onload = function(e) {\n");
      out.write("\t\t\timg_object.attr(\"src\", e.target.result);\n");
      out.write("\t\t};\n");
      out.write("\t\t\n");
      out.write("\t\treader.readAsDataURL(this.files[0]);\n");
      out.write("\t}\n");
      out.write("});\n");
      out.write("    \n");
      out.write("  \n");
      out.write("    \n");
      out.write("   \n");
      out.write("</script>\n");
      out.write("\n");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
