<div class="cont">
  <div class="main-box">
    <div class="log">
      <form action="Register">
        <h2>Register</h2>
       
  <i class="fa fa-fw fa-user" aria-hidden="true"></i>
        <input type="text"
               name="username"
               class="user"
               placeholder="Username"/>
       
        
     <i class="fa fa-list-alt" aria-hidden="true"></i>
        <input type="text"
               name="fullname"
               class="user"
               placeholder="Full nam"/>
        <i class="fa fa-fw fa-lock" aria-hidden="true"></i>
        <input type="password" 
               name="password"
               class="pass"
               placeholder="Password"/>

        <button                class="login">Register</button>
      </form>
           <a href="login.jsp">Login</a>
    </div>
  </div> 
</div>

<link rel="stylesheet" href="path/to/font-awesome/css/font-awesome.min.css">
<link rel="stylesheet prefetch" href="//maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css">

<style>
    
    @import url("https://fonts.googleapis.com/css?family=Raleway:300,400,700");

* {
 
  padding: 0;
  outline: none;
  box-sizing: border-box;
  font-family: "Raleway", sans-serif;
}
a{
    color: #41cac0;
}
body {
  background: #41cac0;
}

.cont {
  position: relative;
  width: 28%;
  height: 550px;
  margin: 10px auto;
}

.main-box {
  width: 100%;
  height: 112%;
  text-align: center;
  background: #1d4a5f;
  box-shadow: 0px 0px 40px 1px #262626;
}

.log {
  position: relative;
  top: 20%;
  width: 100%;
  height: 75%;
  padding: 18px 15px;
}

h2,
.user,
.pass {
  display: block;
}

h2 {
  text-align: center;
  color: #4fdac3;
  font-weight: 600;
  font-size: 30px;
  letter-spacing: 3px;
  margin-bottom: 50px;
}

.user,
.pass {
  width: 85%;
  height: 45px;
  border: none;
  color: white;
  margin: auto;
  padding-left: 40px;
  font-size: 18px;
  font-weight: 500;
  margin-bottom: 25px;
  background: #0e2d3f;
}

input::placeholder {
  color: #eaeaea;
  font-size: 15px;
  font-weight: lighter;
}

.fa {
  position: relative;
  color: #fff;
  font-size: 16px;
  left: -36%;
  top: 33px;
}

.login {
  color: #fff;
  cursor: pointer;
  width: 85%;
  height: 50px;
  border: none;
  font-size: 22px;
  font-weight: 500;
  margin-top: 2%;
  box-shadow: 0px 2px 3px 5px rgba(42, 42, 42, 0.23);
  background: linear-gradient(to right, #4ce3c2, #35c1c0);
}

@media only screen and (min-width: 280px) {
  .cont {
    width: 90%;
  }
}

@media only screen and (min-width: 480px) {
  .cont {
    width: 60%;
  }
}

@media only screen and (min-width: 768px) {
  .cont {
    width: 40%;
  }
}

@media only screen and (min-width: 992px) {
  .cont {
    width: 30%;
  }
}

</style>