
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:if test="${username!=null}">
    
    
<link href="cssmenu/csmenu.css" rel="stylesheet" type="text/css"/>
<script src='https://ajax.googleapis.com/ajax/libs/jquery/1.5.1/jquery.min.js' type='text/javascript'></script>
<script src="cssmenu/jsmenu.js" type="text/javascript"></script>
<link href='https://fonts.googleapis.com/css?family=Raleway:400,700' rel='stylesheet' type='text/css'>
<link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
<div class="header">
     <nav id="fixNav" class="container">
        <a class="fb" href="#"><i class="fa fa-home" aria-hidden="true"></i> </a>
     
        <a class="fl" href="#"><i class="fa fa-bolt" aria-hidden="true"></i></a>
        <a class="tu" href="#"><i class="fa fa-tumblr"></i></a>
        <a  id='SignIn' class="vi" href="#"><i class="fa fa-pencil" aria-hidden="true"></i></a>
    </nav>
    <div class="inner">
        <div class="user-ui">
            <div class="user-menu-toggle">
                <div class="profile-img" style="background-image:url(${avatar})"></div>
                <span id="btn1" class="simple-arrow fa fa-chevron-up"></span>
            </div>
            <!-- User menu -->
            <div id='comment-editor' style='display:none !important' class="user-menu">
                <div class="user-info">
                    <div class="profile-img"  style="background-image:url(${avatar})"></div>
                    <h3 class="name">${username}</h3>
                    <div class="ui btn normal"> <a href="SignOut" class="button delete"><span>Sign Out</span></a>
                    </div>
                </div>
                <div class="menu-nav">
                    <li></span>Personal page</li>
                    <li ><a id='ed' href="#">Edit</a></li>
                    <li>My comment
                    </li>
                    <li></span>My post</li>
                </div>
            </div>
        </div>
    </div>
</div>





    <!DOCTYPE html>

    <head>
         <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    </head>
        <style>
            #fixNav{


                margin-left: 42%;
    float: left;
    margin-top: 10px;
              width: 300px;
            }
        </style>

        <link rel="stylesheet" href="path/to/font-awesome/css/font-awesome.min.css">
        <link rel="stylesheet prefetch" href="//maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css">

   

    <style>
        .container {
            width: 100%;
            display: table;
            margin: 0;
        }

        nav a {
            display: table-cell;
            color: white;
            width: 14.2%;
            text-align: center;
            font-size: 2em;
            padding: .5em;
        }
        @media (max-width: 768px) {
            nav a {
                font-size: 1.5em;
            }
        }
        nav a:first-child {
            border-radius: 5px 0 0 5px;
        }
        @media (max-width: 768px) {
            nav a:first-child {
                border-radius: 0;
            }
        }
        nav a:last-child {
            border-radius: 0px 5px 5px 0px;
        }
        @media (max-width: 768px) {
            nav a:last-child {
                border-radius: 0;
            }
        }

        nav a:first-child:nth-last-child(1) {
            width: 100%;
        }

        /* two items */
        nav a:first-child:nth-last-child(2),
        nav a:first-child:nth-last-child(2) ~ a {
            width: 50%;
        }

        /* three items */
        nav a:first-child:nth-last-child(3),
        nav a:first-child:nth-last-child(3) ~ a {
            width: 33.3333%;
        }

        /* four items */
        nav a:first-child:nth-last-child(4),
        nav a:first-child:nth-last-child(4) ~ a {
            width: 25%;
        }

        /* five items */
        nav a:first-child:nth-last-child(5),
        nav a:first-child:nth-last-child(5) ~ a {
            width: 20%;
        }

        /* six items */
        nav a:first-child:nth-last-child(6),
        nav a:first-child:nth-last-child(6) ~ a {
            width: 16.666%;
        }

        /* seven items */
        nav a:first-child:nth-last-child(7),
        nav a:first-child:nth-last-child(7) ~ a {
            width: 14.285%;
        }

        /* eight items */
        nav a:first-child:nth-last-child(8),
        nav a:first-child:nth-last-child(8) ~ a {
            width: 12.5%;
        }

        /*
        Assigning link background colors and setting the hover and active states. For quick adding of buttons, perform a search + replace on the 2 letter variable name.
        */
        nav a.fb {
            background: #3B5998;
            text-shadow: 1px 1px 0px #263961;
        }
        nav a.fb:hover {
            color: #3055a3 !important;
            background: #2d4373;
        }
        nav a.fb:active {
            color: #3B5998;
            background: #1e2e4f;
        }

        nav a.tw {
            background: #55ACEE;
            text-shadow: 1px 1px 0px #1689e0;
        }
        nav a.tw:hover {
            color: #4cadf7 !important;
            background: #2795e9;
        }
        nav a.tw:active {
            color: #55ACEE;
            background: #147bc9;
        }

        nav a.gp {
            background: #DD4B39;
            text-shadow: 1px 1px 0px #ac2d1e;
        }
        nav a.gp:hover {
            color: #e9422d !important;
            background: #c23321;
        }
        nav a.gp:active {
            color: #DD4B39;
            background: #96271a;
        }

        nav a.ig {
            background: #517FA4;
            text-shadow: 1px 1px 0px #385771;
        }
        nav a.ig:hover {
            color: #4580b0 !important;
            background: #406582;
        }
        nav a.ig:active {
            color: #517FA4;
            background: #2f4a60;
        }

        nav a.fl {
            background: #FF0084;
            text-shadow: 1px 1px 0px #b3005c;
        }
        nav a.fl:hover {
            color: #ff0084 !important;
            background: #cc006a;
        }
        nav a.fl:active {
            color: #FF0084;
            background: #99004f;
        }

        nav a.tu {
            background: #32506D;
            text-shadow: 1px 1px 0px #1a2a39;
        }
        nav a.tu:hover {
            color: #2a5075 !important;
            background: #22364a;
        }
        nav a.tu:active {
            color: #32506D;
            background: #121d27;
        }

        nav a.vi {
            background: #00BF8F;
            text-shadow: 1px 1px 0px #007356;
        }
        nav a.vi:hover {
            color: #00bf8f !important;
            background: #008c69;
        }
        nav a.vi:active {
            color: #00BF8F;
            background: #005943;
        }

    </style>

</head>
<body>

    <jsp:useBean class="com.wpsj.model.getAll" id="finder1" scope="request"/>


    <div class="container">
        <ul class="posts">

            <c:forEach items="${finder1.products}" var="product">
                <c:if test="${product.post_username!=null}">
                <li class="post">
                    <div class="post-content">
                        <header><a href="PersonalPage?username=${product.post_username}">${product.post_username}</a></header>
                            <c:if test = "${product.post_image!=''}">
                            <img src="${product.post_image}"/>
                        </c:if>

                        <footer> <p id="v">

                                ${product.post_content}


                            </p>
                            <a href="https://kamilciesla.pl"><i class="fa fa-heart" aria-hidden="true"></i></a>
                            <a href="Comment?id=${product.post_id}"><i class="fa fa-comment" aria-hidden="true"></i></a>
                            <a href="https://twitter.com/@kamciesla"><i class="fa fa-paper-plane" aria-hidden="true"></i></a></footer>
                    </div><img src="${product.post_avatar}" class="post-avatar" href="http://kamilciesla.pl"></img>
                </li>
                </c:if>
            </c:forEach>

        </ul>

    </div>


    <link href="csspost/newcss.css" rel="stylesheet" type="text/css"/>
    <style>
        #v{
            font-family: "Helvetica Neue", HelveticaNeue, Helvetica, Arial, sans-serif;
            size: 14px;
            line-height: 21px;

            color: #444444;
            width: 514px;
            word-wrap: break-word;
        }


        footer  {
            width: 200px;
        }</style>
</body>












<script src="csscomment/jquery-3.3.1.js" type="text/javascript"></script>
<script src="csscomment/jquery-3.3.1.min.js" type="text/javascript"></script>
<div style='display:none !important' class='login'>
    <button class='close' id='close'></button>
    <form action="FileUploadServlet" method="post" enctype="multipart/form-data">

      
<textarea name="content" onkeyup="textAreaAdjust(this)"></textarea>
        <input name="file" type='file' onchange="readURL(this);" />
        <img  class="v" style='display:none !important' id="blah" src="" alt="your image" />
        <input type="submit" value="Post">
    </form>
</div>

<style>
     textarea {
        margin-top: 31px;
    margin-left: 18px;
    margin-bottom: 22px;
    width: 450px;
    min-height: 60px;
    height: auto;
    /* margin: 22px auto; */
    padding: 20px;
    display: block;
    overflow: hidden;
    border-color: #ECEFF1;
    border-radius: 2px;
    font: 300 14px/20px 'Roboto',sans-serif;
    resize: none;
}

    #blah{
        max-width:100%;
    }
    input[type=file]{
        width: 100%;
        padding:10px;
        background:#2d2d2d;}
</style>

<script>
    function textAreaAdjust(o) {
  o.style.height = "1px";
  o.style.height = (5+o.scrollHeight)+"px";
}
    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('.v').show();
                $('#blah')
                        .attr('src', e.target.result);
            };
            reader.readAsDataURL(input.files[0]);
        }
    }
</script>
<link href="csscreatepost/createpost.css" rel="stylesheet" type="text/css"/>
<script src="csscreatepost/jscreatepost.js" type="text/javascript"></script>





























<script src="csscomment/jquery-3.3.1.js" type="text/javascript"></script>
<script src="csscomment/jquery-3.3.1.min.js" type="text/javascript"></script>
<div style='display:none !important' class='prototype'>
    <button class='close' id='close1'></button>
   <form action="UpProfile" method="post" enctype="multipart/form-data">

<div class="media-container">
	<span class="media-overlay">
		<input name="file" type='file' id="media-input" >
		<i class="glyphicon glyphicon-edit media-icon"></i>
	</span>
	<figure class="media-object">
		<img class="img-object" src="${avatar}">
	</figure>
</div>
<div class="media-control">
	<a class="edit-profile">Edit Profile</a>
	<a class="save-profile">Save Profile</a>
</div>
        
        
       
        <input type="text" value="${username}" id="input" placeholder="Name">
 
 
 
  <input type="submit" value="Save" id="input-submit">   
        
    </form>
</div>

<link href="csseditprototype/prototype.css" rel="stylesheet" type="text/css"/>
<script src="csseditprototype/jsprototype.js" type="text/javascript"></script>
<script>

    var btn_save = $(".save-profile"),
	btn_edit = $(".edit-profile"),
	img_object = $(".img-object"),
	overlay = $(".media-overlay"),
	media_input = $("#media-input");

btn_save.hide(0);
overlay.hide(0);

$('.edit-profile').on('click', function() {
	$(this).hide(0);
	$(this).siblings().fadeToggle(300);
	overlay.fadeToggle(300);
});

media_input.change(function() {
	if (this.files && this.files[0]) {
		var reader = new FileReader();
		
		reader.onload = function(e) {
			img_object.attr("src", e.target.result);
		};
		
		reader.readAsDataURL(this.files[0]);
	}
});
    
  
    
   
</script>

</c:if>
