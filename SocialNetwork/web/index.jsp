
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

    
    
<link href="cssmenu/csmenu.css" rel="stylesheet" type="text/css"/>
<script src='https://ajax.googleapis.com/ajax/libs/jquery/1.5.1/jquery.min.js' type='text/javascript'></script>
<script src="cssmenu/jsmenu.js" type="text/javascript"></script>
<link href='https://fonts.googleapis.com/css?family=Raleway:400,700' rel='stylesheet' type='text/css'>
<link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
<div class="header">
     <nav id="fixNav" class="container">
        <a class="fb" href="#"><i class="fa fa-home" aria-hidden="true"></i> </a>
     
        <a class="fl" href="#"><i class="fa fa-bolt" aria-hidden="true"></i></a>
        <a class="tu" href="#"><i class="fa fa-tumblr"></i></a>
        <a  id='SignIn' class="vi" href="#"><i class="fa fa-pencil" aria-hidden="true"></i></a>
    </nav>
    <div class="inner">
        <div class="user-ui">
            <div class="user-menu-toggle">
                <div class="profile-img" style="background-image:url(${avatar})"></div>
                <span id="btn1" class="simple-arrow fa fa-chevron-up"></span>
            </div>
            <!-- User menu -->
            <div id='comment-editor' style='display:none !important' class="user-menu">
                <div class="user-info">
                    <div class="profile-img"  style="background-image:url(${avatar})"></div>
                    <h3 class="name">${username}</h3>
                    <div class="ui btn normal"> <a href="SignOut" class="button delete"><span>Sign Out</span></a>
                    </div>
                </div>
                <div class="menu-nav">
                    <li></span>Personal page</li>
                    <li ><a id='ed' href="#">Edit</a></li>
                    <li>My comment
                    </li>
                    <li></span>My post</li>
                </div>
            </div>
        </div>
    </div>
</div>





    <!DOCTYPE html>

    <head>
         <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    </head>
        <style>
            #fixNav{


                margin-left: 42%;
    float: left;
    margin-top: 10px;
              width: 300px;
            }
        </style>

        <link rel="stylesheet" href="path/to/font-awesome/css/font-awesome.min.css">
        <link rel="stylesheet prefetch" href="//maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css">

   

    <style>
        .container {
            width: 100%;
            display: table;
            margin: 0;
        }

        nav a {
            display: table-cell;
            color: white;
            width: 14.2%;
            text-align: center;
            font-size: 2em;
            padding: .5em;
        }
        @media (max-width: 768px) {
            nav a {
                font-size: 1.5em;
            }
        }
        nav a:first-child {
            border-radius: 5px 0 0 5px;
        }
        @media (max-width: 768px) {
            nav a:first-child {
                border-radius: 0;
            }
        }
        nav a:last-child {
            border-radius: 0px 5px 5px 0px;
        }
        @media (max-width: 768px) {
            nav a:last-child {
                border-radius: 0;
            }
        }

        nav a:first-child:nth-last-child(1) {
            width: 100%;
        }

        /* two items */
        nav a:first-child:nth-last-child(2),
        nav a:first-child:nth-last-child(2) ~ a {
            width: 50%;
        }

        /* three items */
        nav a:first-child:nth-last-child(3),
        nav a:first-child:nth-last-child(3) ~ a {
            width: 33.3333%;
        }

        /* four items */
        nav a:first-child:nth-last-child(4),
        nav a:first-child:nth-last-child(4) ~ a {
            width: 25%;
        }

        /* five items */
        nav a:first-child:nth-last-child(5),
        nav a:first-child:nth-last-child(5) ~ a {
            width: 20%;
        }

        /* six items */
        nav a:first-child:nth-last-child(6),
        nav a:first-child:nth-last-child(6) ~ a {
            width: 16.666%;
        }

        /* seven items */
        nav a:first-child:nth-last-child(7),
        nav a:first-child:nth-last-child(7) ~ a {
            width: 14.285%;
        }

        /* eight items */
        nav a:first-child:nth-last-child(8),
        nav a:first-child:nth-last-child(8) ~ a {
            width: 12.5%;
        }

        /*
        Assigning link background colors and setting the hover and active states. For quick adding of buttons, perform a search + replace on the 2 letter variable name.
        */
        nav a.fb {
            background: #3B5998;
            text-shadow: 1px 1px 0px #263961;
        }
        nav a.fb:hover {
            color: #3055a3 !important;
            background: #2d4373;
        }
        nav a.fb:active {
            color: #3B5998;
            background: #1e2e4f;
        }

        nav a.tw {
            background: #55ACEE;
            text-shadow: 1px 1px 0px #1689e0;
        }
        nav a.tw:hover {
            color: #4cadf7 !important;
            background: #2795e9;
        }
        nav a.tw:active {
            color: #55ACEE;
            background: #147bc9;
        }

        nav a.gp {
            background: #DD4B39;
            text-shadow: 1px 1px 0px #ac2d1e;
        }
        nav a.gp:hover {
            color: #e9422d !important;
            background: #c23321;
        }
        nav a.gp:active {
            color: #DD4B39;
            background: #96271a;
        }

        nav a.ig {
            background: #517FA4;
            text-shadow: 1px 1px 0px #385771;
        }
        nav a.ig:hover {
            color: #4580b0 !important;
            background: #406582;
        }
        nav a.ig:active {
            color: #517FA4;
            background: #2f4a60;
        }

        nav a.fl {
            background: #FF0084;
            text-shadow: 1px 1px 0px #b3005c;
        }
        nav a.fl:hover {
            color: #ff0084 !important;
            background: #cc006a;
        }
        nav a.fl:active {
            color: #FF0084;
            background: #99004f;
        }

        nav a.tu {
            background: #32506D;
            text-shadow: 1px 1px 0px #1a2a39;
        }
        nav a.tu:hover {
            color: #2a5075 !important;
            background: #22364a;
        }
        nav a.tu:active {
            color: #32506D;
            background: #121d27;
        }

        nav a.vi {
            background: #00BF8F;
            text-shadow: 1px 1px 0px #007356;
        }
        nav a.vi:hover {
            color: #00bf8f !important;
            background: #008c69;
        }
        nav a.vi:active {
            color: #00BF8F;
            background: #005943;
        }

    </style>

</head>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
       <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Using AJAX with JSp/Servlet with JSON return type</title>
        
        <link href="csscomment/newcss.css" rel="stylesheet" type="text/css"/>
        <style>
            html, body {
                background: #41cac0;
                font-family: "PT Sans", "Helvetica Neue", "Helvetica", "Roboto", "Arial", sans-serif;
                color: #555f77;
                -webkit-font-smoothing: antialiased;
            }

            input, textarea {
                outline: none;
                border: none;
                display: block;
                margin: 0;
                padding: 0;
                -webkit-font-smoothing: antialiased;
                font-family: "PT Sans", "Helvetica Neue", "Helvetica", "Roboto", "Arial", sans-serif;
                font-size: 1rem;
                color: #555f77;
            }
            input::-webkit-input-placeholder, textarea::-webkit-input-placeholder {
                color: #ced2db;
            }
            input::-moz-placeholder, textarea::-moz-placeholder {
                color: #ced2db;
            }
            input:-moz-placeholder, textarea:-moz-placeholder {
                color: #ced2db;
            }
            input:-ms-input-placeholder, textarea:-ms-input-placeholder {
                color: #ced2db;
            }

            p {
                line-height: 1.3125rem;
            }

            .comments {
                   margin: 2.5rem auto 0;
    max-width: 37rem;
    padding: 0 1.25rem;
    margin-left: 428px;
            }

            .comment-wrap {
                margin-bottom: 1.25rem;
                display: table;
                width: 100%;
                min-height: 5.3125rem;
            }

            .photo {
                padding-top: 0.625rem;
                display: table-cell;
                width: 3.5rem;
            }
            .photo .avatar {
                height: 2.25rem;
                width: 2.25rem;
                border-radius: 50%;
                background-size: cover;
            }

            .comment-block {
                margin-top: 10px;
                margin-bottom: 20px;
                width: 100%;
                float: left;
                padding: 1rem;
                background-color: #fff;
                display: table-cell;
                vertical-align: top;
                border-radius: 0.1875rem;
                -webkit-box-shadow: 0 1px 3px 0 rgba(0, 0, 0, 0.08);
                box-shadow: 0 1px 3px 0 rgba(0, 0, 0, 0.08);
            }
            .comment-block textarea {
                width: 100%;
                resize: none;
            }

            .comment-text {
                margin-bottom: 1.25rem;
            }

            .bottom-comment {
                color: #acb4c2;
                font-size: 0.875rem;
            }

            .comment-date {
                float: left;
                margin-top: -21px;
                margin-left: 18px;
            }

            .comment-actions {
                float: right;
            }
            .comment-actions li {
                display: inline;
                margin: -2px;
                cursor: pointer;
            }
            .comment-actions li.complain {
                padding-right: 0.75rem;
                border-right: 1px solid #e1e5eb;
            }
            .comment-actions li.reply {
                padding-left: 0.75rem;
                padding-right: 0.125rem;
            }
            .comment-actions li:hover {
                color: #0095ff;
            }

        </style>
      
        <script src="csscomment/ajax.js" type="text/javascript"></script>
        <script src="csscomment/jquery-3.3.1.min.js" type="text/javascript"></script>
        
        <script type="text/javascript">

            function myFunction(obj) {
                var pro = $('#cm').val();
                var i_d =  obj.id;
                $.ajax({
                    type: "GET",
                    data: {comment: pro,id: i_d},
                    url: "GetDistrictFromProvince",

                    success: function (responseJson) {
                         var c=$('cc').height();
                       jQuery('html,body').animate({scrollTop: jQuery('.comment-block').offset().top},c);
                        if(responseJson!=null){
                        var $dis = $('#cc');
                        var dem = 0;
                        var html = '<div  class="comment-block">';
                        $.each(responseJson, function (key, value) {
                            dem++;
                            if (dem == 1) {
                                html += '<h5>' + value + '</h5>';
                            }
                            if (dem == 3) {


                                html += '<div class="bottom-comment">';
                                html += '<div class="bottom-comment">'
                                html += '  <div class="comment-date">' + value + '</div>';

                                html += " </div></div></div>";
                                 $('#cc').append(html);
                            }
                            if (dem == 2) {
                                html += '<p class="comment-text">' + (value) + ' </div>';




                            }

                        });
                        
                        
                       
                    }
                    }})
            }


        </script>
    </head>
    <body id="bd">
       
       
        <div id="district">
            <p id="p1"></p>  
            <p id="p2"></p>  
        </div>



        



    <link href="csspost/newcss.css" rel="stylesheet" type="text/css"/>
        
        
        
         <jsp:useBean class="com.wpsj.model.getAll" id="post" scope="request"/>
         

<div class="container">
  <ul class="posts">
      
      <c:forEach items="${post.p}" var="product">
     
    <li class="post">
      <div class="post-content">
        <header><a href="https://twitter.com/@kamciesla">${product.post_username}</a></header>
        <c:if test = "${product.post_image!=''}">
        <img src="${product.post_image}"/>
        </c:if>
        
        <footer> <p id="v">
            
      ${product.post_content}
            
            
            </p>
            <a href="https://kamilciesla.pl"><i class="fa fa-heart" aria-hidden="true"></i></a>
            <a href="https://pl.linkedin.com/in/kamilciesla"><i class="fa fa-comment" aria-hidden="true"></i></a>
            <a href="https://twitter.com/@kamciesla"><i class="fa fa-paper-plane" aria-hidden="true"></i></a></footer>
      </div><img src="${product.post_image}" class="post-avatar" href="http://kamilciesla.pl"></img>
    </li>
   
  
  </ul>
  
</div>
        
        
        
    
    
    
        
        

        <div id="cc" class="comments">


            <div class="photo">
                <div class="avatar" style="background-image: url('${avatar}')"></div>
            </div>
            <div class="comment-block">
                <form action="">
                    <textarea name="comment" id="cm" cols="30" rows="3" placeholder="Add comment..."></textarea>
                </form>
            </div>

 <input id="${product.post_id}" type = "button" value = "Click Here" onClick = "myFunction(this)">
  </c:forEach>
 
 
 
    <jsp:useBean class="com.wpsj.model.getAll" id="cm" scope="request"/>
    <c:forEach items="${cm.cm}" var="comment">
         <div class="photo">
                <div class="avatar" style="background-image: url('${comment.cm_avatar}')"></div>
            </div>
        <div  class="comment-block">
            
             <h5>${comment.cm_username}</h5>
                           <div class="bottom-comment">
                                <div class="bottom-comment">
                                    
                                      <p class="comment-text">${comment.cm_content}</div>
                                <div class="comment-date"> ${comment.cm_time} </div>
                                
                              
        </div>
                               
        </div>
    </c:forEach>
 
<div id="footer">  
        
</div><!--END #footer--> 

        </div>


<style>#footer {
  
   bottom: 0;
  
    /* Height of the footer */
   background:#6cf;
}
</style>

    </div>

</div>




















