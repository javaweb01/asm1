



<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>


<link href="cssmenu/csmenu.css" rel="stylesheet" type="text/css"/>
<script src='https://ajax.googleapis.com/ajax/libs/jquery/1.5.1/jquery.min.js' type='text/javascript'></script>
<script src="cssmenu/jsmenu.js" type="text/javascript"></script>
<link href='https://fonts.googleapis.com/css?family=Raleway:400,700' rel='stylesheet' type='text/css'>
<link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
<div class="header">
     <nav id="fixNav" class="container">
        <a class="fb" href="newjsp.jsp"><i class="fa fa-home" aria-hidden="true"></i> </a>
     
        <a class="fl" href="#"><i class="fa fa-bolt" aria-hidden="true"></i></a>
        <a class="tu" href="#"><i class="fa fa-tumblr"></i></a>
        <a  id='SignIn' class="vi" href="#"><i class="fa fa-pencil" aria-hidden="true"></i></a>
    </nav>
    <div class="inner">
        <div class="user-ui">
            <div class="user-menu-toggle">
                <div class="profile-img" style="background-image:url(${avatar})"></div>
                <span id="btn1" class="simple-arrow fa fa-chevron-up"></span>
            </div>
            <!-- User menu -->
            <div id='comment-editor' style='display:none !important' class="user-menu">
                <div class="user-info">
                    <div class="profile-img"  style="background-image:url(${avatar})"></div>
                    <h3 class="name">${username}</h3>
                    <div class="ui btn normal"> <a href="SignOut" class="button delete"><span>Sign Out</span></a>
                    </div>
                </div>
                <div class="menu-nav">
                    <li></span>Personal page</li>
                    <li ><a id='ed' href="#">Edit</a></li>
                    <li>My comment
                    </li>
                    <li></span>My post</li>
                </div>
            </div>
        </div>
    </div>
</div>





    <!DOCTYPE html>

    <head>
         <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    </head>
        <style>
            #fixNav{


                margin-left: 42%;
    float: left;
    margin-top: 10px;
              width: 300px;
            }
        </style>

        <link rel="stylesheet" href="path/to/font-awesome/css/font-awesome.min.css">
        <link rel="stylesheet prefetch" href="//maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css">

   

    <style>
        .container {
            width: 100%;
            display: table;
            margin: 0;
        }

        nav a {
            display: table-cell;
            color: white;
            width: 14.2%;
            text-align: center;
            font-size: 2em;
            padding: .5em;
        }
        @media (max-width: 768px) {
            nav a {
                font-size: 1.5em;
            }
        }
        nav a:first-child {
            border-radius: 5px 0 0 5px;
        }
        @media (max-width: 768px) {
            nav a:first-child {
                border-radius: 0;
            }
        }
        nav a:last-child {
            border-radius: 0px 5px 5px 0px;
        }
        @media (max-width: 768px) {
            nav a:last-child {
                border-radius: 0;
            }
        }

        nav a:first-child:nth-last-child(1) {
            width: 100%;
        }

        /* two items */
        nav a:first-child:nth-last-child(2),
        nav a:first-child:nth-last-child(2) ~ a {
            width: 50%;
        }

        /* three items */
        nav a:first-child:nth-last-child(3),
        nav a:first-child:nth-last-child(3) ~ a {
            width: 33.3333%;
        }

        /* four items */
        nav a:first-child:nth-last-child(4),
        nav a:first-child:nth-last-child(4) ~ a {
            width: 25%;
        }

        /* five items */
        nav a:first-child:nth-last-child(5),
        nav a:first-child:nth-last-child(5) ~ a {
            width: 20%;
        }

        /* six items */
        nav a:first-child:nth-last-child(6),
        nav a:first-child:nth-last-child(6) ~ a {
            width: 16.666%;
        }

        /* seven items */
        nav a:first-child:nth-last-child(7),
        nav a:first-child:nth-last-child(7) ~ a {
            width: 14.285%;
        }

        /* eight items */
        nav a:first-child:nth-last-child(8),
        nav a:first-child:nth-last-child(8) ~ a {
            width: 12.5%;
        }

        /*
        Assigning link background colors and setting the hover and active states. For quick adding of buttons, perform a search + replace on the 2 letter variable name.
        */
        nav a.fb {
            background: #3B5998;
            text-shadow: 1px 1px 0px #263961;
        }
        nav a.fb:hover {
            color: #3055a3 !important;
            background: #2d4373;
        }
        nav a.fb:active {
            color: #3B5998;
            background: #1e2e4f;
        }

        nav a.tw {
            background: #55ACEE;
            text-shadow: 1px 1px 0px #1689e0;
        }
        nav a.tw:hover {
            color: #4cadf7 !important;
            background: #2795e9;
        }
        nav a.tw:active {
            color: #55ACEE;
            background: #147bc9;
        }

        nav a.gp {
            background: #DD4B39;
            text-shadow: 1px 1px 0px #ac2d1e;
        }
        nav a.gp:hover {
            color: #e9422d !important;
            background: #c23321;
        }
        nav a.gp:active {
            color: #DD4B39;
            background: #96271a;
        }

        nav a.ig {
            background: #517FA4;
            text-shadow: 1px 1px 0px #385771;
        }
        nav a.ig:hover {
            color: #4580b0 !important;
            background: #406582;
        }
        nav a.ig:active {
            color: #517FA4;
            background: #2f4a60;
        }

        nav a.fl {
            background: #FF0084;
            text-shadow: 1px 1px 0px #b3005c;
        }
        nav a.fl:hover {
            color: #ff0084 !important;
            background: #cc006a;
        }
        nav a.fl:active {
            color: #FF0084;
            background: #99004f;
        }

        nav a.tu {
            background: #32506D;
            text-shadow: 1px 1px 0px #1a2a39;
        }
        nav a.tu:hover {
            color: #2a5075 !important;
            background: #22364a;
        }
        nav a.tu:active {
            color: #32506D;
            background: #121d27;
        }

        nav a.vi {
            background: #00BF8F;
            text-shadow: 1px 1px 0px #007356;
        }
        nav a.vi:hover {
            color: #00bf8f !important;
            background: #008c69;
        }
        nav a.vi:active {
            color: #00BF8F;
            background: #005943;
        }
.header {
    position: fixed;
    z-index: 100000;
    margin-top: -30px;
}
#fixNav {
    margin-left: 38%;}
    </style>

</head>





































 <jsp:useBean class="com.wpsj.model.getAll" id="cm" scope="request"/>



<div id="container">

  <a class="img" href="#">
  <div class="img__overlay img__overlay--red">Hey!</div>
  <img src="${avatar1}" />
</a>
<div id="content">
		<h1>${username1}</h1>
		  
                 <div class="container">
        <ul class="posts">
            <c:forEach items="${cm.postUsername}" var="product">
                <c:if test="${product.post_username!=null}">
                <li class="post">
                    <div class="post-content">
                        <header><a href="https://twitter.com/@kamciesla">${product.post_username}</a></header>
                            <c:if test = "${product.post_image!=''}">
                            <img src="${product.post_image}"/>
                        </c:if>
                        <footer> <p id="v">

                                ${product.post_content}
                            </p>
                            <a href="https://kamilciesla.pl"><i class="fa fa-heart" aria-hidden="true"></i></a>
                            <a href="Comment?id=${product.post_id}"><i class="fa fa-comment" aria-hidden="true"></i></a>
                            <a href="https://twitter.com/@kamciesla"><i class="fa fa-paper-plane" aria-hidden="true"></i></a></footer>
                            <c:if test = "${username==username1}">
                            <a href="DeletePost?idpost=${product.post_id}"><i class="fa fa-trash" aria-hidden="true"></i></a></footer>
        </c:if>
                    </div>
                </li>
                </c:if>
            </c:forEach>

        </ul>

    </div>
	</div>
</div>



  <link rel="stylesheet" href="path/to/font-awesome/css/font-awesome.min.css">
        <link rel="stylesheet prefetch" href="//maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css">


<style>
 @import url(https://fonts.googleapis.com/css?family=Raleway:400,500,600);
body {
  background: url("http://emilcarlsson.se/assets/forest-green-blue-gradient.jpg") no-repeat center center fixed;
  -webkit-background-size: cover;
  -moz-background-size: cover;
  -o-background-size: cover;
  background-size: cover;
  font-family: 'Raleway', sans-serif;
}

#container {
  width: 90%;
  margin: 30px auto 0 auto;
  background: #FFF;
  border-radius: 10px;
  box-shadow: 0 0 30px rgba(0, 0, 0, 0.3);
  -webkit-box-shadow: 0 0 30px rgba(0, 0, 0, 0.3);
  overflow: hidden;
}
#container #hero-img {
  width: 100%;
  height: 200px;
  background: url("http://emilcarlsson.se/assets/forest.jpg") no-repeat center center;
  background-size: cover;
}
#container #profile-img {
  width: 160px;
  height: 160px;
  margin: -80px auto 0 auto;
  border: 6px solid #FFF;
  border-radius: 50%;
  box-shadow: 0 0 5px rgba(90, 90, 90, 0.3);
}
#container #profile-img img {
  width: 100%;
  background: #FFF;
  border-radius: 50%;
}
#container #content {
  text-align: center;
  width: 90%;
  margin: 0 auto;
  padding: 0 0 50px 0;
  
}
#container #content h1 {
  font-size: 29px;
  font-weight: 500;
  margin: 20px 0 0 0;
}
#container #content p {
  font-size: 18px;
  font-weight: 400;
  line-height: 1.4;
  color: #666;
  margin: 15px 0 40px 0;
}
#container #content a {
  color: #CCC;
  font-size: 14px;
  margin: 0 10px;
  transition: color .3s ease-in-out;
  -webkit-transition: color .3s ease-in-out;
}
#container #content a:hover {
  color: #2ecc71;
}


   .post {
    margin-top: 20px;
    
    position: relative;
}
    
    </style>
    
    
    <style>
        
        
        body {
  font: bold 13px Helvetica, Arial, sans-serif;
  
      background: #41cac0;
}

a {
  color: #a2a2a2;
  text-decoration: none;
  -webkit-transition: color .3s ease;
  transition: color .3s ease;
}
footer a {
  margin-right: 10px;
}


a:hover {
  color: #666;
}

.container {
  max-width: 650px;
  width: 100%;
  margin: 0 auto;
}

.no-posts {
  padding: 50px 10px 30px 100px;
  text-align: center;
  color: #fff;
}

.posts {
  
  list-style: none;
  margin: 20px auto;
  padding: 0;
  width: 100%;
    margin-top: 80px;
}
.posts .post {
  margin-top: 20px;
  
  position: relative;
}
.posts .post-avatar {
  background: #fff url("http://on.be.net/1Noq5Sf") center/100%;
  height: 80px;
  width: 80px;
  border-radius: 4px;
  position: absolute;
  left: -100px;
  top: 0;
}
.posts .post-avatar.post-avatar--fixed {
  position: fixed;
  left: 50%;
  top: 20px;
  margin-left: -325px;
}
.posts .post-avatar.post-avatar--absolute {
  position: absolute;
  bottom: 0;
  left: 0;
  margin-left: -100px;
  top: auto;
}
.posts .post-content {
  background: #fff;
  border-radius: 4px;
  width: 100%;
}
.posts .post-content img {
  width: 100%;
}
.posts .post-content header, .posts .post-content footer {
  padding: 15px 20px;
}













.img {
  border-radius: 100%;
  color: #fafafa;
  display: -webkit-box;
  display: -ms-flexbox;
  display: flex;
  -webkit-box-flex: 0;
  -ms-flex: 0 0 200px;
  flex: 0 0 200px;
  height: 200px;
  -webkit-box-pack: center;
  -ms-flex-pack: center;
  justify-content: center;
  overflow: hidden;
  position: relative;
  width: 200px;
      margin-left: 500px;
    margin-top: 100px;
}
.img img {
  height: 100%;
}

.i
.img {
  -webkit-animation: fadeIn 0.5s;
  animation: fadeIn 0.5s;
  border: 4px solid #c0c0c0;
 
     
}


@-webkit-keyframes fadeIn {
  from {
    opacity: 0;
  }
  to {
    opacity: 1;
  }
}
@keyframes fadeIn {
  from {
    opacity: 0;
  }
  to {
    opacity: 1;
  }
}



    </style>